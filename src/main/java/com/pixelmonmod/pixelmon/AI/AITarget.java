/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.AI;

import com.pixelmonmod.pixelmon.config.PixelmonItems;
import com.pixelmonmod.pixelmon.entities.npcs.NPCTrainer;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumBossMode;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public abstract class AITarget extends EntityAIBase
{
	protected EntityCreature taskOwner;
	protected float targetDistance;
	protected boolean shouldCheckSight;
	private boolean nearbyOnly;
	private int targetSearchStatus;
	private int targetSearchDelay;
	private int targetUnseenTicks;

	public AITarget(EntityPixelmon par1EntityLiving, float par2, boolean par3)
	{
		this(par1EntityLiving, par2, par3, false);
		this.setMutexBits(3);
	}

	public AITarget(EntityCreature entity, float par2, boolean par3, boolean par4)
	{
		this.targetSearchStatus = 0;
		this.targetSearchDelay = 0;
		this.targetUnseenTicks = 0;
		this.taskOwner = entity;
		this.targetDistance = par2;
		this.shouldCheckSight = par3;
		this.nearbyOnly = par4;
	}

	public boolean continueExecuting()
	{
		if (this.taskOwner instanceof EntityPixelmon)
		{
			EntityPixelmon var1 = (EntityPixelmon) this.taskOwner;
			if (var1.battleController != null || var1.getBossMode() != EnumBossMode.NotBoss)
			{
				return false;
			}
		}
		else if (((NPCTrainer) this.taskOwner).battleController != null)
		{
			return false;
		}

		EntityLivingBase arg1 = this.taskOwner.getAttackTarget();
		if (arg1 != null && arg1.isEntityAlive() && this.taskOwner.getDistanceSqToEntity(arg1) <= (double) (this.targetDistance * this.targetDistance))
		{
			if (this.shouldCheckSight)
			{
				if (this.taskOwner instanceof NPCTrainer)
				{
					if (this.taskOwner.getEntitySenses().canSee(arg1) && this.checkAngle(arg1))
					{
						this.targetUnseenTicks = 0;
					}
					else if (++this.targetUnseenTicks > 60)
					{
						return false;
					}
				}
				else if (this.taskOwner.getEntitySenses().canSee(arg1))
				{
					this.targetUnseenTicks = 0;
				}
				else if (++this.targetUnseenTicks > 60)
				{
					return false;
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	private boolean checkAngle(EntityLivingBase var1)
	{
		Vec3d look = this.taskOwner.getLook(1.0F);
		Vec3d toTarget = new Vec3d(this.taskOwner.posX - var1.posX, this.taskOwner.posY - var1.posY, this.taskOwner.posZ - var1.posZ);
		double top = look.xCoord * toTarget.xCoord + look.yCoord * toTarget.yCoord + look.zCoord * toTarget.zCoord;
		double A = Math.sqrt(Math.pow(look.xCoord, 2.0D) + Math.pow(look.yCoord, 2.0D) + Math.pow(look.zCoord, 2.0D));
		double B = Math.sqrt(Math.pow(toTarget.xCoord, 2.0D) + Math.pow(toTarget.yCoord, 2.0D) + Math.pow(toTarget.zCoord, 2.0D));
		double angle = Math.acos(top / (A * B));
		return Math.abs(angle - 3.141592653589793D) <= 0.2D;
	}

	public void startExecuting()
	{
		this.targetSearchStatus = 0;
		this.targetSearchDelay = 0;
		this.targetUnseenTicks = 0;
	}

	public void resetTask()
	{
		this.taskOwner.setAttackTarget((EntityLivingBase) null);
	}

	protected boolean isSuitableTarget(EntityLivingBase entity, boolean par2)
	{
		if (entity != null && (entity instanceof EntityPixelmon || entity instanceof EntityPlayer) && entity != this.taskOwner && entity.isEntityAlive())
		{
			
			
			
			if(entity instanceof EntityPixelmon)
			{
				EntityPixelmon poke = (EntityPixelmon) entity;
				if(poke.specialDungeonAvoidDespawn)
				{
					return false;
				}
			}
			
			
			
			
			
			if (entity instanceof EntityPlayer && this.taskOwner instanceof NPCTrainer)
			{
				NPCTrainer entityBounds = (NPCTrainer) this.taskOwner;
				EntityPlayer thisBounds = (EntityPlayer) entity;
				ItemStack thisOwner = thisBounds.getHeldItemMainhand();
				if (thisOwner != null && thisOwner.getItem() == PixelmonItems.trainerEditor && entityBounds.getAIMode().doesEngage() || !entityBounds.canStartBattle(thisBounds, false))
				{
					return false;
				}
			}

			AxisAlignedBB arg6 = entity.getEntityBoundingBox();
			AxisAlignedBB arg7 = this.taskOwner.getEntityBoundingBox();
			if (arg6.maxY > arg7.minY && arg6.minY < arg7.maxY)
			{
				EntityLivingBase arg8 = null;
				if (this.taskOwner instanceof EntityPixelmon)
				{
					arg8 = ((EntityPixelmon) this.taskOwner).getOwner();
				}

				if (arg8 != null)
				{
					if (entity instanceof EntityPixelmon)
					{
						EntityPixelmon optstorage = (EntityPixelmon) entity;
						if (optstorage.getOwner() == arg8 || optstorage.spawner != null || optstorage.isInRanchBlock)
						{
							return false;
						}
					}

					if (entity == ((EntityTameable) this.taskOwner).getOwner())
					{
						return false;
					}
				}
				else
				{
					if (entity instanceof EntityPlayer && !par2 && ((EntityPlayer) entity).capabilities.disableDamage)
					{
						return false;
					}

					if (entity instanceof EntityPlayer)
					{
						Optional arg9 = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP) entity);
						if (arg9.isPresent() && ((PlayerStorage) arg9.get()).countAblePokemon() == 0)
						{
							return false;
						}
					}
				}

				if (this.taskOwner.isWithinHomeDistanceCurrentPosition() && (!this.shouldCheckSight || this.taskOwner.getEntitySenses().canSee(entity)))
				{
					if (this.nearbyOnly)
					{
						if (--this.targetSearchDelay <= 0)
						{
							this.targetSearchStatus = 0;
						}

						if (this.targetSearchStatus == 0)
						{
							this.targetSearchStatus = this.canEasilyReach(entity) ? 1 : 2;
						}

						if (this.targetSearchStatus == 2)
						{
							return false;
						}
					}

					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private boolean canEasilyReach(EntityLivingBase par1EntityLiving)
	{
		this.targetSearchDelay = 10 + this.taskOwner.getRNG().nextInt(5);
		Path var2 = this.taskOwner.getNavigator().getPathToEntityLiving(par1EntityLiving);
		if (var2 == null)
		{
			return false;
		}
		else
		{
			PathPoint var3 = var2.getFinalPathPoint();
			if (var3 == null)
			{
				return false;
			}
			else
			{
				int var4 = var3.xCoord - MathHelper.floor_double(par1EntityLiving.posX);
				int var5 = var3.zCoord - MathHelper.floor_double(par1EntityLiving.posZ);
				return (double) (var4 * var4 + var5 * var5) <= 2.25D;
			}
		}
	}
}