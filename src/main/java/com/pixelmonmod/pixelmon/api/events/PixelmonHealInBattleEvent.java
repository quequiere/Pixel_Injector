package com.pixelmonmod.pixelmon.api.events;

import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity3HasStats;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonHealInBattleEvent extends Event
{

	public Entity3HasStats entity3HasStats;
	public BattleControllerBase bc;
	private int i;
	
	
	public PixelmonHealInBattleEvent(Entity3HasStats entity3HasStats, BattleControllerBase bc, int i)
	{
		this.entity3HasStats=entity3HasStats;
		this.bc=bc;
		this.i=i;
	}


	public int getI()
	{
		return i;
	}


	public void setI(int i)
	{
		this.i = i;
	}
	

	
}
