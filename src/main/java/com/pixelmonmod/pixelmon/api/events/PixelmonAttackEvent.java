package com.pixelmonmod.pixelmon.api.events;

import com.pixelmonmod.pixelmon.battles.attacks.DamageTypeEnum;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity6CanBattle;

import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonAttackEvent extends Event
{
	private Entity6CanBattle target;
	private PixelmonWrapper source;
	private float damage;
	private DamageTypeEnum damageType;
	
	public PixelmonAttackEvent(Entity6CanBattle target, PixelmonWrapper source, float damage, DamageTypeEnum damageType)
	{
		this.target = target;
		this.source = source;
		this.damage = damage;
		this.damageType = damageType;
	}
	
	public void setDamage(float f)
	{
		this.damage=f;
	}

	public Entity6CanBattle getTarget()
	{
		return target;
	}

	public PixelmonWrapper getSource()
	{
		return source;
	}

	public float getDamage()
	{
		return damage;
	}

	public DamageTypeEnum getDamageType()
	{
		return damageType;
	}
	
	
}
