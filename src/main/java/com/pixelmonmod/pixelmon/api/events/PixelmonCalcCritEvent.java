package com.pixelmonmod.pixelmon.api.events;

import com.pixelmonmod.pixelmon.battles.attacks.EffectBase;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonCalcCritEvent extends Event
{

	public EffectBase e;
	public PixelmonWrapper user; 
	public PixelmonWrapper target;
	private double crit;
	
	public PixelmonCalcCritEvent(EffectBase e, PixelmonWrapper user, PixelmonWrapper target, double crit)
	{
		this.e=e;
		this.user=user;
		this.target=target;
		this.crit=crit;
	}

	public double getCrit()
	{
		return crit;
	}

	public void setCrit(double crit)
	{
		this.crit = crit;
	}
	
	
	
	
}
