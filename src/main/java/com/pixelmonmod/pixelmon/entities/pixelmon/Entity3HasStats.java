/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.entities.pixelmon;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.api.events.PixelmonCalcCritEvent;
import com.pixelmonmod.pixelmon.api.events.PixelmonFaintEvent;
import com.pixelmonmod.pixelmon.api.events.PixelmonHealInBattleEvent;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.database.DatabaseAbilities;
import com.pixelmonmod.pixelmon.database.DatabaseStats;
import com.pixelmonmod.pixelmon.database.SpawnLocation;
import com.pixelmonmod.pixelmon.entities.npcs.registry.DropItemRegistry;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity2HasModel;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity6CanBattle;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.AbilityBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.ComingSoon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.BaseStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.ExtraStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.FlyingParameters;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.FriendShip;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Gender;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.IVStore;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Level;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Stats;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.SwimmingParameters;
import com.pixelmonmod.pixelmon.entities.pixelmon.tickHandlers.TickHandlerBase;
import com.pixelmonmod.pixelmon.enums.EnumEggGroup;
import com.pixelmonmod.pixelmon.enums.EnumMegaPokemon;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.pokedex.EnumPokedexRegisterStatus;
import com.pixelmonmod.pixelmon.pokedex.Pokedex;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import net.minecraft.block.material.Material;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public abstract class Entity3HasStats extends Entity2HasModel
{
	protected Level level;
	public Stats stats;
	public BaseStats baseStats;
	public FriendShip friendship;
	public ArrayList<EnumType> type = new ArrayList();
	public float length;
	public boolean doesLevel = true;
	private static Map<Integer, BaseStats> baseStatsStore = new HashMap(750);
	private static Map<String, BaseStats> baseStatsByName = new HashMap(750);
	public ExtraStats extraStats = null;
	private AbilityBase ability;
	private Integer abilitySlot;
	private TickHandlerBase tickHandler;
	public boolean isMega;
	static EnumMap<EnumPokemon, Integer> forms = new EnumMap(EnumPokemon.class);
	private FlyingParameters bossFlyingParameters = null;
	private SwimmingParameters bossSwimmingParameters = null;

	public Entity3HasStats(World par1World)
	{
		super(par1World);
		this.dataManager.register(EntityPixelmon.dwScale, Integer.valueOf(1000));
		this.dataManager.register(EntityPixelmon.dwForm, Integer.valueOf(-1));
		this.stats = new Stats();
		if (this instanceof EntityPixelmon)
		{
			this.level = new Level((EntityPixelmon) this);
			this.friendship = new FriendShip((EntityPixelmon) this);
		}

		this.dataManager.register(EntityPixelmon.dwMaxHP, Integer.valueOf(10));
	}

	public int getCatchRate()
	{
		float c = (float) this.baseStats.catchRate;
		return (int) c;
	}

	public boolean isEntityInsideOpaqueBlock()
	{
		if (super.isEntityInsideOpaqueBlock())
		{
			if (this.worldObj.getBlockState(this.getPosition().add(1, 0, 0)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(1, 0, 0))) == Material.AIR)
			{
				this.setPosition(this.posX + 1.0D, this.posY, this.posZ);
			}

			if (this.worldObj.getBlockState(this.getPosition().add(-1, 0, 0)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(-1, 0, 0))) == Material.AIR)
			{
				this.setPosition(this.posX - 1.0D, this.posY, this.posZ);
			}

			if (this.worldObj.getBlockState(this.getPosition().add(0, 1, 0)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(0, 1, 0))) == Material.AIR)
			{
				this.setPosition(this.posX, this.posY + 1.0D, this.posZ);
			}

			if (this.worldObj.getBlockState(this.getPosition().add(0, -1, 0)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(0, -1, 0))) == Material.AIR)
			{
				this.setPosition(this.posX, this.posY - 1.0D, this.posZ);
			}

			if (this.worldObj.getBlockState(this.getPosition().add(0, 0, 1)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(0, 0, 1))) == Material.AIR)
			{
				this.setPosition(this.posX, this.posY, this.posZ + 1.0D);
			}

			if (this.worldObj.getBlockState(this.getPosition().add(0, 0, -1)).getBlock().getMaterial(this.worldObj.getBlockState(this.getPosition().add(0, 0, -1))) == Material.AIR)
			{
				this.setPosition(this.posX, this.posY, this.posZ - 1.0D);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.55D);
	}

	protected void init(String name)
	{
		super.init(name);
		if (this.baseStats == null)
		{
			this.baseStats = (BaseStats) getBaseStats(name, this.getForm()).get();
		}

		if (this.baseStats == null && !this.worldObj.isRemote)
		{
			this.setDead();
		}
		else
		{
			this.extraStats = this.getExtraStats(name);
			if (this.baseStats.hasEggGroup(EnumEggGroup.Undiscovered))
			{
				this.stats.IVs = IVStore.CreateNewIVs3Perfect();
			}
			else
			{
				this.stats.IVs = IVStore.CreateNewIVs();
			}

			this.setType();
			this.length = this.baseStats.length;
			this.chooseRandomGender();
			this.isImmuneToFire = this.type.contains(EnumType.Fire);
			if (!(this instanceof EntityStatue) && this.level.getLevel() == -1)
			{
				int spawnLevelRange = this.baseStats.spawnLevelRange;
				int spawnLevel = this.baseStats.spawnLevel;
				if (spawnLevelRange <= 0)
				{
					this.level.setLevel(spawnLevel);
				}
				else
				{
					this.level.setLevel(spawnLevel + this.rand.nextInt(spawnLevelRange));
				}

				this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue((double) this.stats.HP);
				this.setHealth((float) this.stats.HP);
			}

			if (this.getAbility() == null)
			{
				this.giveAbility();
			}

			if (this.getForm() == -1 && this.hasForms())
			{
				this.setForm(getRandomForm(this.baseStats.pokemon));
			}

			this.setSize(this.baseStats.width, this.baseStats.height + this.baseStats.hoverHeight);
			this.setPosition(this.posX, this.posY, this.posZ);
			if (this instanceof EntityPixelmon)
			{
				this.tickHandler = TickHandlerBase.getTickHandler(this);
			}
		}

	}

	private ExtraStats getExtraStats(String name)
	{
		return ExtraStats.getExtraStats(name);
	}

	float getMoveSpeed()
	{
		return 0.3F + (1.0F - (200.0F - (float) this.stats.Speed) / 200.0F) * 0.3F;
	}

	public static BaseStats getBaseStatsFromStore(String name)
	{
		return getBaseStatsFromStore(name, -1);
	}

	public static BaseStats getBaseStatsFromStore(String name, int form)
	{
		if (form == -1)
		{
			return (BaseStats) baseStatsByName.get(name);
		}
		else
		{
			BaseStats noForm = null;
			Iterator arg2 = baseStatsStore.values().iterator();

			while (true)
			{
				BaseStats aBaseStatsStore;
				do
				{
					do
					{
						do
						{
							do
							{
								if (!arg2.hasNext())
								{
									return noForm;
								}

								aBaseStatsStore = (BaseStats) arg2.next();
							}
							while (aBaseStatsStore == null);
						}
						while (aBaseStatsStore.pixelmonName == null);
					}
					while (!aBaseStatsStore.pixelmonName.equalsIgnoreCase(name));

					if (aBaseStatsStore.form == form)
					{
						return aBaseStatsStore;
					}
				}
				while (aBaseStatsStore.form != -1 && aBaseStatsStore.form != 0);

				noForm = aBaseStatsStore;
			}
		}
	}

	public static BaseStats getBaseStats(int index)
	{
		Iterator arg0 = baseStatsStore.values().iterator();

		BaseStats b;
		do
		{
			if (!arg0.hasNext())
			{
				return null;
			}

			b = (BaseStats) arg0.next();
		}
		while (b == null || b.nationalPokedexNumber != index);

		return b;
	}

	public static BaseStats getBaseStatsFromDBID(int id)
	{
		return (BaseStats) baseStatsStore.get(Integer.valueOf(id));
	}

	public static Optional<BaseStats> getBaseStats(String name)
	{
		return getBaseStats(name, -1);
	}

	public static Optional<BaseStats> getBaseStats(String name, int form)
	{
		BaseStats baseStats = getBaseStatsFromStore(name, form);
		BaseStats noForm = null;
		if (baseStats != null)
		{
			if (baseStats.form == form || form == -1)
			{
				return Optional.of(baseStats);
			}

			noForm = baseStats;
		}

		Optional optional = DatabaseStats.getBaseStats(name, form);
		if (optional.isPresent())
		{
			baseStats = (BaseStats) optional.get();
			if (!baseStatsStore.containsKey(Integer.valueOf(baseStats.id)))
			{
				baseStatsStore.put(Integer.valueOf(baseStats.id), baseStats);
			}

			if (form == -1 && !baseStatsByName.containsKey(baseStats.pixelmonName))
			{
				baseStatsByName.put(baseStats.pixelmonName, baseStats);
			}

			return Optional.of(baseStats);
		}
		else if (noForm != null)
		{
			return Optional.of(noForm);
		}
		else
		{
			return Optional.empty();
		}
	}

	public void onDeath(DamageSource par1DamageSource)
	{
		if (this.getOwner() != null)
		{
			this.friendship.onFaint();
			Pixelmon.EVENT_BUS.post(new PixelmonFaintEvent((EntityPlayerMP) this.getOwner(), (EntityPixelmon) this));
		}

		boolean showDeathMessages = this.worldObj.getGameRules().getBoolean("showDeathMessages");
		this.worldObj.getGameRules().setOrCreateGameRule("showDeathMessages", "false");
		super.onDeath(par1DamageSource);
		this.worldObj.getGameRules().setOrCreateGameRule("showDeathMessages", showDeathMessages ? "true" : "false");
	}

	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
	{
		if (this instanceof EntityStatue)
		{
			return false;
		}
		else
		{
			if (par1DamageSource.getSourceOfDamage() == this.getOwner())
			{
				this.friendship.hurtByOwner();
			}

			return super.attackEntityFrom(par1DamageSource, par2);
		}
	}

	private void setType()
	{
		this.type.clear();
		this.type.add(this.baseStats.type1);
		if (this.baseStats.type2 != EnumType.Mystery)
		{
			this.type.add(this.baseStats.type2);
		}

	}

	public void evolve(String evolveTo)
	{
		super.evolve(evolveTo);
		float oldHp = (float) this.stats.HP;
		float oldHealth = this.getHealth();
		this.baseStats = (BaseStats) getBaseStats(evolveTo).get();
		this.setType();
		String[] oldAbilities = DatabaseAbilities.getAbilities(this.getPokemonName());
		String[] newAbilities = DatabaseAbilities.getAbilities(evolveTo);
		Integer newSlot = this.getAbilitySlot();
		if (this.getAbilitySlot().intValue() == 0 && oldAbilities[1] == null && newAbilities[1] != null)
		{
			newSlot = Integer.valueOf(RandomHelper.getRandomNumberBetween(0, 1));
		}

		if (this.getAbilitySlot().intValue() == 1 && newAbilities[1] == null)
		{
			newSlot = Integer.valueOf(0);
		}

		this.setAbilitySlot(newSlot);
		String chosenAbility = newAbilities[newSlot.intValue()];
		this.setAbility(chosenAbility);
		if (!this.hasForms() && this.getForm() > -1)
		{
			this.setForm(-1);
		}

		this.updateStats();
		float newHealth = (float) this.stats.HP;
		if (oldHp != 0.0F)
		{
			newHealth = oldHealth / oldHp * (float) this.stats.HP;
		}

		this.setHealth((float) ((int) Math.ceil((double) newHealth)));
		Optional storage = this.getStorage();
		if (storage.isPresent())
		{
			Pokedex pokedex = ((PlayerStorage) storage.get()).pokedex;
			pokedex.set(Pokedex.nameToID(this.getPokemonName()), EnumPokedexRegisterStatus.caught);
			pokedex.sendToPlayer((EntityPlayerMP) pokedex.owner);
		}

		if (this.getOwner() != null)
		{
			this.update(new EnumUpdateType[] { EnumUpdateType.Name, EnumUpdateType.Stats, EnumUpdateType.Ability });
		}

	}

	protected boolean canDespawn()
	{
		return true;
	}

	public void setDead()
	{
		if (this.getOwner() != null && !this.worldObj.isRemote)
		{
			Optional storage = this.getStorage();
			if (storage.isPresent())
			{
				((PlayerStorage) storage.get()).retrieve((EntityPixelmon) this);
			}
		}

		super.setDead();
	}

	public void fall(float distance, float damageMultiplier)
	{
		if (this.baseStats != null)
		{
			if (this.getSpawnLocation() == SpawnLocation.Water)
			{
				return;
			}

			if (this.baseStats.canFly)
			{
				return;
			}
		}

		super.fall(distance, damageMultiplier);
	}

	public boolean canBreatheUnderwater()
	{
		return this.baseStats == null || this.getSpawnLocation() == SpawnLocation.Water || this.baseStats.type1 == EnumType.Water || this.baseStats.type2 == EnumType.Water;
	}

	public void setHealth(float par1)
	{
		super.setHealth(par1);
		this.updateHealth();
	}

	public void healEntityBy(int i)
	{
		BattleControllerBase bc = ((Entity6CanBattle) this).battleController;
		if ((float) i + this.getHealth() > this.getMaxHealth())
		{
			i = (int) (this.getMaxHealth() - this.getHealth());
		}
		
		//quequiere
		if(bc!=null)
		{
			PixelmonHealInBattleEvent healevent = new PixelmonHealInBattleEvent(this,bc,i);
			Pixelmon.EVENT_BUS.post(healevent);
			i=healevent.getI();
		}
		
		

		if (i != 0)
		{
			if (bc != null && !bc.simulateMode)
			{
				bc.sendHealPacket((EntityPixelmon) this, i);
			}

			if (bc == null || !bc.simulateMode)
			{
				this.setHealth(this.getHealth() + (float) i);
			}
		}

	}

	public void updateHealth()
	{
		if (this.stats != null && this.getHealth() > this.getMaxHealth())
		{
			super.setHealth(this.getMaxHealth());
		}

		if (this.getHealth() < 0.0F)
		{
			super.setHealth(0.0F);
		}

		if (this.getOwner() != null && !this.worldObj.isRemote)
		{
			this.update(new EnumUpdateType[] { EnumUpdateType.HP });
		}

	}

	public void setPixelmonScale(float scale)
	{
		this.dataManager.set(EntityPixelmon.dwScale, Integer.valueOf((int) (scale * 1000.0F)));
	}

	public float getPixelmonScale()
	{
		return (float) ((Integer) this.dataManager.get(EntityPixelmon.dwScale)).intValue() / 1000.0F;
	}

	public void setPosition(double x, double y, double z)
	{
		this.posX = x;
		this.posY = y;
		this.posZ = z;
		float scale = 1.0F;
		float scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3F : 1.0F;
		if (this.isInitialised)
		{
			scale = this.getPixelmonScale() * scaleFactor * this.getScaleFactor();
		}

		float halfWidth;
		if (this.baseStats != null)
		{
			halfWidth = this.baseStats.width * scale / 2.0F;
			this.setEntityBoundingBox(new AxisAlignedBB(x - (double) halfWidth, y, z - (double) halfWidth, x + (double) halfWidth, y + (double) this.baseStats.height * (double) scale + (double) this.baseStats.hoverHeight, z + (double) halfWidth));
		}
		else
		{
			halfWidth = this.width * scale / 2.0F;
			this.setEntityBoundingBox(new AxisAlignedBB(x - (double) halfWidth, y, z - (double) halfWidth, x + (double) halfWidth, y - this.getYOffset() + (double) this.height * (double) scale, z + (double) halfWidth));
		}

	}

	public void updateStats()
	{
		if (this.level != null)
		{
			this.stats.setLevelStats(this.getNature(), this.baseStats, this.level.getLevel());
			this.dataManager.set(EntityPixelmon.dwMaxHP, Integer.valueOf(this.stats.HP));
			this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue((double) this.stats.HP);
			this.updateHealth();
		}

	}

	public Level getLvl()
	{
		return this.level;
	}

	public void onUpdate()
	{
		if (!(this instanceof EntityStatue))
		{
			if (this.getOwner() != null && !this.worldObj.isRemote)
			{
				this.friendship.tick();
			}

			if (this.hasOwner() && this.getOwner() == null && !this.worldObj.isRemote)
			{
				this.setDead();
			}
		}
		else
		{
			this.rotationYawHead = this.rotationYaw;
		}

		if (this.tickHandler != null)
		{
			this.tickHandler.tick(this.worldObj);
		}

		super.onUpdate();
	}

	public boolean writeToNBTOptional(NBTTagCompound par1nbtTagCompound)
	{
		return this instanceof EntityStatue ? super.writeToNBTOptional(par1nbtTagCompound) : !this.isDead && !this.hasOwner() && ((EntityPixelmon) this).blockOwner == null && (!this.canDespawn() || PixelmonConfig.writeEntitiesToWorld) && super.writeToNBTOptional(par1nbtTagCompound);
	}

	public void writeEntityToNBT(NBTTagCompound nbt)
	{
		super.writeEntityToNBT(nbt);
		if (!(this instanceof EntityStatue))
		{
			if (!this.isMega)
			{
				this.stats.writeToNBT(nbt);
			}

			this.level.writeToNBT(nbt);
			this.friendship.writeToNBT(nbt);
			nbt.setBoolean("DoesLevel", this.doesLevel);
			if (this.extraStats != null)
			{
				this.extraStats.writeToNBT(nbt);
			}

			if (this.ability != null && !this.isMega)
			{
				String form = this.ability.getName();
				if (form.equals("Coming Soon"))
				{
					nbt.setString("Ability", ((ComingSoon) this.ability).getTrueAbility());
				}
				else
				{
					nbt.setString("Ability", form);
				}
			}

			if (this.abilitySlot != null)
			{
				nbt.setInteger("AbilitySlot", this.abilitySlot.intValue());
			}
		}

		if (!this.isMega)
		{
			int form1 = this.getForm();
			nbt.setInteger("Variant", form1);
		}

	}

	public void getNBTTags(HashMap<String, Class> tags)
	{
		super.getNBTTags(tags);
		this.stats.getNBTTags(tags);
		this.level.getNBTTags(tags);
		this.friendship.getNBTTags(tags);
		ExtraStats.getNBTTags(tags);
		tags.put("DoesLevel", Boolean.class);
		tags.put("Ability", String.class);
		tags.put("AbilitySlot", Integer.class);
		tags.put("Health", Float.class);
		tags.put("Variant", Integer.class);
	}

	public void readEntityFromNBT(NBTTagCompound nbt)
	{
		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue((double) nbt.getInteger("StatsHP"));
		super.readEntityFromNBT(nbt);
		if (!(this instanceof EntityStatue))
		{
			this.stats.readFromNBT(nbt);
			this.friendship.readFromNBT(nbt);
			this.doesLevel = !nbt.hasKey("DoesLevel") || nbt.getBoolean("DoesLevel");
			if (this.extraStats != null)
			{
				this.extraStats.readFromNBT(nbt);
			}

			try
			{
				if (nbt.hasKey("Ability"))
				{
					String e = nbt.getString("Ability");

					try
					{
						this.ability = (AbilityBase) AbilityBase.getAbility(e).get();
					}
					catch (Exception arg5)
					{
						if (e.equals("ComingSoon"))
						{
							if (nbt.hasKey("AbilitySlot"))
							{
								int tempAbilitySlot = nbt.getInteger("AbilitySlot");
								if (this.baseStats.abilities != null && this.baseStats.abilities[tempAbilitySlot] != null)
								{
									e = this.baseStats.abilities[tempAbilitySlot];
									this.setAbility(e);
								}
							}
						}
						else
						{
							this.giveAbility();
						}
					}
				}
			}
			catch (Exception arg6)
			{
				System.out.println("Didn\'t have an Ability; giving it one.");
				this.giveAbility();
			}

			try
			{
				if (nbt.hasKey("AbilitySlot"))
				{
					this.abilitySlot = Integer.valueOf(nbt.getInteger("AbilitySlot"));
				}
				else
				{
					this.giveAbilitySlot();
				}
			}
			catch (Exception arg4)
			{
				System.out.println("Didn\'t have an Ability slot; giving it one.");
				this.giveAbilitySlot();
			}

			if (nbt.hasKey("primaryType"))
			{
				this.type.clear();
				this.type.add(EnumType.parseOrNull(nbt.getShort("primaryType")));
				if (nbt.hasKey("secondaryType") && nbt.getShort("secondaryType") != -1)
				{
					this.type.add(EnumType.parseOrNull(nbt.getShort("secondaryType")));
				}
			}
		}

		if (nbt.hasKey("Variant"))
		{
			this.setForm(nbt.getInteger("Variant"), false);
		}

		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.55D);
	}

	public int getMaxSpawnedInChunk()
	{
		return this.rand.nextInt(this.baseStats.maxGroupSize - this.baseStats.minGroupSize + 1) + this.baseStats.minGroupSize;
	}

	private void giveAbility()
	{
		String abilityName = "";
		if (this.baseStats.abilities != null && (PixelmonConfig.hiddenAbilityRate <= 0 || this.baseStats.abilities[2] == null || RandomHelper.getRandomNumberBetween(1, PixelmonConfig.hiddenAbilityRate) != 1))
		{
			if (this.baseStats.abilities[1] != null)
			{
				this.abilitySlot = Integer.valueOf(RandomHelper.getRandomNumberBetween(0, 1));
			}
			else
			{
				this.abilitySlot = Integer.valueOf(0);
			}

			abilityName = this.baseStats.abilities[this.abilitySlot.intValue()];
			this.setAbility(abilityName);
		}
		else
		{
			this.abilitySlot = Integer.valueOf(2);
			if (this.baseStats.abilities[this.abilitySlot.intValue()] != null)
			{
				abilityName = this.baseStats.abilities[this.abilitySlot.intValue()];
				this.setAbility(abilityName);
			}
			else
			{
				this.ability = new ComingSoon(abilityName);
			}
		}

	}

	public void giveAbilitySlot()
	{
		Integer slot = DatabaseAbilities.getSlotForAbility(this.baseStats.id, this.ability.getName());
		if (slot == null)
		{
			this.abilitySlot = Integer.valueOf(0);
		}
		else
		{
			this.abilitySlot = slot;
		}

	}

	public AbilityBase getAbility()
	{
		return this.ability;
	}

	public Integer getAbilitySlot()
	{
		return this.abilitySlot;
	}

	public void setAbility(AbilityBase newAbility)
	{
		this.ability = newAbility;
	}

	public void setAbility(String abilityName)
	{
		try
		{
			Optional e = AbilityBase.getAbility(abilityName);
			if (e.isPresent())
			{
				this.ability = (AbilityBase) e.get();
			}
			else
			{
				this.ability = new ComingSoon(abilityName);
			}
		}
		catch (Exception arg2)
		{
			Pixelmon.LOGGER.info("Entity3HasStats.setAbility() is erroring and it is weird.");
			if (PixelmonConfig.printErrors)
			{
				arg2.printStackTrace();
			}
		}

	}

	public void setRandomAbilityUniform()
	{
		ArrayList randomAbilities = new ArrayList();
		String[] arg1 = this.baseStats.abilities;
		int arg2 = arg1.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			String ability = arg1[arg3];
			if (ability != null)
			{
				randomAbilities.add(ability);
			}
		}

		if (!randomAbilities.isEmpty())
		{
			this.setAbility((String) RandomHelper.getRandomElementFromList(randomAbilities));
		}

	}

	public void setAbilitySlot(Integer slot)
	{
		this.abilitySlot = Integer.valueOf(Math.max(0, Math.min(2, slot.intValue())));
	}

	public boolean hasForms()
	{
		return this.baseStats != null && hasForms(this.baseStats.pokemon);
	}

	public static boolean hasForms(EnumPokemon pokemon)
	{
		return forms.containsKey(pokemon);
	}

	public static boolean hasForms(String name)
	{
		Optional pokemon = EnumPokemon.getFromName(name);
		return pokemon.isPresent() && hasForms((EnumPokemon) pokemon.get());
	}

	public int getNumForms()
	{
		return this.hasForms() ? ((Integer) forms.get(this.baseStats.pokemon)).intValue() : 0;
	}

	public static int getNumForms(String name)
	{
		Optional pokemon = EnumPokemon.getFromName(name);
		return pokemon.isPresent() ? getNumForms((EnumPokemon) pokemon.get()) : 0;
	}

	public static int getNumForms(EnumPokemon pokemon)
	{
		return hasForms(pokemon) ? ((Integer) forms.get(pokemon)).intValue() : 0;
	}

	public void setForm(int form)
	{
		this.setForm(form, true);
	}

	public void setForm(int form, boolean update)
	{
		boolean needsUpdate = false;
		if (this.baseStats != null)
		{
			boolean formChange = false;
			if (!this.hasForms() && !this.isMega)
			{
				if (this.baseStats.pokemon.hasMega() && this instanceof EntityStatue)
				{
					int numForms = EnumMegaPokemon.getMega(this.baseStats.pokemon).numMegaForms + 1;
					if (form > numForms || form == 0 || form < -1)
					{
						form = -1;
					}

					formChange = true;
				}
				else
				{
					form = -1;
				}
			}
			else
			{
				if (!this.isMega)
				{
					form = Math.min(Math.max(0, form), this.getNumForms() - 1);
				}

				formChange = true;
			}

			if (formChange)
			{
				this.baseStats = (BaseStats) getBaseStats(this.baseStats.pokemon.name, form).get();
				this.setType();
				if (this.isMega)
				{
					this.setAbility(this.baseStats.abilities[form > -1 ? 0 : this.getAbilitySlot().intValue()]);
				}

				needsUpdate = true;
			}
		}

		this.dataManager.set(EntityPixelmon.dwForm, Integer.valueOf(form));
		if (this instanceof EntityPixelmon && update && needsUpdate)
		{
			this.updateStats();
			this.update(new EnumUpdateType[] { EnumUpdateType.Stats });
		}

	}

	public int getForm()
	{
		return ((Integer) this.dataManager.get(EntityPixelmon.dwForm)).intValue();
	}

	public int getFormIncludeTransformed()
	{
		return this.transformed ? this.transformedForm : this.getForm();
	}

	public static int getRandomForm(EnumPokemon pokemon)
	{
		return hasForms(pokemon) ? RandomHelper.rand.nextInt(getNumForms(pokemon)) : -1;
	}

	public int getPartyPosition()
	{
		Optional storage = this.getStorage();
		return storage.isPresent() ? ((PlayerStorage) storage.get()).getPosition(this.getPokemonId()) : -1;
	}

	public boolean isAvailableGeneration()
	{
		return isAvailableGeneration(this.getPokemonName());
	}

	public static boolean isAvailableGeneration(String name)
	{
		int dexNumber = getPokedexNumber(name);
		return dexNumber != -1 && isAvailableGeneration(dexNumber);
	}

	public static boolean isAvailableGeneration(int dexNumber)
	{
		return dexNumber <= 151 ? PixelmonConfig.Gen1 : (dexNumber <= 251 ? PixelmonConfig.Gen2 : (dexNumber <= 386 ? PixelmonConfig.Gen3 : (dexNumber <= 493 ? PixelmonConfig.Gen4 : (dexNumber <= 649 ? PixelmonConfig.Gen5 : (dexNumber <= 721 ? PixelmonConfig.Gen6 : false)))));
	}

	public static int getPokedexNumber(String name)
	{
		Optional pokemonOptional = getBaseStats(name);
		return pokemonOptional.isPresent() ? ((BaseStats) pokemonOptional.get()).nationalPokedexNumber : -1;
	}

	public void chooseRandomGender()
	{
		this.gender = getRandomGender(this.baseStats);
	}

	public static Gender getRandomGender(BaseStats baseStats)
	{
		return baseStats.malePercent < 0 ? Gender.None : (RandomHelper.rand.nextInt(100) < baseStats.malePercent ? Gender.Male : Gender.Female);
	}

	public SpawnLocation getDefaultSpawnLocation()
	{
		return this.baseStats != null && this.baseStats.spawnLocations != null && this.baseStats.spawnLocations.length > 0 ? this.baseStats.spawnLocations[0] : SpawnLocation.Land;
	}

	public boolean isSingleType()
	{
		return this.type.size() == 1 || this.type.size() == 2 && this.type.get(1) == null;
	}

	public boolean isSingleType(EnumType type)
	{
		return this.isSingleType() && this.type.contains(type);
	}

	public FlyingParameters getFlyingParameters()
	{
		if (this.bossFlyingParameters != null)
		{
			return this.bossFlyingParameters;
		}
		else
		{
			if (this.isBossPokemon() && this.baseStats.pokemon.hasMega())
			{
				this.bossFlyingParameters = DropItemRegistry.getBossPokemon(this.baseStats.pokemon).flyingParameters;
				if (this.bossFlyingParameters != null)
				{
					return this.bossFlyingParameters;
				}
			}

			return this.baseStats.flyingParameters;
		}
	}

	public SwimmingParameters getSwimmingParameters()
	{
		if (this.bossSwimmingParameters != null)
		{
			return this.bossSwimmingParameters;
		}
		else if (this.isBossPokemon() && this.baseStats.pokemon.hasMega())
		{
			this.bossSwimmingParameters = DropItemRegistry.getBossPokemon(this.baseStats.pokemon).swimmingParameters;
			return this.bossSwimmingParameters;
		}
		else
		{
			return this.baseStats.swimmingParameters;
		}
	}

	static
	{
		forms.put(EnumPokemon.Unown, Integer.valueOf(28));
		forms.put(EnumPokemon.Burmy, Integer.valueOf(3));
		forms.put(EnumPokemon.Wormadam, Integer.valueOf(3));
		forms.put(EnumPokemon.Castform, Integer.valueOf(4));
		forms.put(EnumPokemon.Deoxys, Integer.valueOf(4));
	}
}