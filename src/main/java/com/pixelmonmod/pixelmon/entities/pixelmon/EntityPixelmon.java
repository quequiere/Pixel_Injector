/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.entities.pixelmon;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.WorldHelper;
import com.pixelmonmod.pixelmon.api.events.PixelmonUpdateEvent;
import com.pixelmonmod.pixelmon.api.interactions.IInteraction;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.blocks.spawning.TileEntityPixelmonSpawner;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.database.SpawnLocation;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity10CanBreed;
import com.pixelmonmod.pixelmon.entities.pixelmon.EnumAggression;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Illusion;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.AIHelper;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.EvolutionQuery;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.BaseStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.Evolution;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.EvolutionType;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class EntityPixelmon extends Entity10CanBreed
{
	public static final DataParameter<String> dwName;
	public static final DataParameter<String> dwNickname;
	public static final DataParameter<Integer> dwPokemonID1;
	public static final DataParameter<Integer> dwPokemonID2;
	public static final DataParameter<Integer> dwExp;
	public static final DataParameter<String> dwTrainerName;
	public static final DataParameter<Integer> dwTextures;
	public static final DataParameter<Byte> dwTextureVariation;
	public static final DataParameter<Integer> dwScale;
	public static final DataParameter<Integer> dwMaxHP;
	public static final DataParameter<Integer> dwBossMode;
	public static final DataParameter<Integer> dwNature;
	public static final DataParameter<Integer> dwGrowth;
	public static final DataParameter<Integer> dwNumInteractions;
	public static final DataParameter<Integer> dwForm;
	public static final DataParameter<Integer> dwLevel;
	public static final DataParameter<Integer> dwTransformation;
	public static final DataParameter<Integer> dwSpawnLocation;
	public static final DataParameter<Integer> dwNumBreedingLevels;
	public boolean playerOwned = false;
	public int legendaryTicks = -1;
	public int despawnCounter = -1;
	public static int despawnRadius;
	public static int TICKSPERSECOND;
	public static int intMinTicksToDespawn;
	public static int intMaxTicksToDespawn;
	public boolean getTimings = false;
	public long startTime = 0L;
	public static ArrayList<IInteraction> interactionList;
	public boolean canMove = true;
	public boolean stopRender = false;
	public TileEntityPixelmonSpawner spawner = null;
	public ArrayList<UUID> cameraCapturedPlayers = new ArrayList();
	
	//quequiere
	public boolean specialDungeonAvoidDespawn = false;

	public EntityPixelmon(World world)
	{
		super(world);
		if (world.isRemote)
		{
			setRenderDistanceWeight(PixelmonConfig.renderDistanceWeight);
		}

		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false, new Class[0]));
		((PathNavigateGround) this.getNavigator()).setCanSwim(false);
	}

	public void init(String name)
	{
		super.init(name);
	}

	public boolean isCreatureType(EnumCreatureType type, boolean forSpawnCount)
	{
		return type == EnumCreatureType.WATER_CREATURE && this.getSpawnLocation() == SpawnLocation.Water ? true : type == EnumCreatureType.CREATURE;
	}

	public void onDeath(DamageSource damagesource)
	{
		if (!this.worldObj.isRemote)
		{
			super.onDeath(damagesource);
			if (this.getOwner() != null)
			{
				if (this.battleController == null)
				{
					this.isFainted = true;
				}

				this.setHealth(0.0F);
				this.catchInPokeball();
			}
			else
			{
				this.setDead();
			}
		}

	}

	public boolean processInteract(EntityPlayer player, EnumHand hand, ItemStack itemstack)
	{
		if (player instanceof EntityPlayerMP && itemstack != null)
		{
			Iterator arg3 = interactionList.iterator();

			while (arg3.hasNext())
			{
				IInteraction i = (IInteraction) arg3.next();
				if (i.processInteract(this, player, hand, itemstack))
				{
					return true;
				}
			}
		}

		return super.processInteract(player, hand, itemstack);
	}

	public void catchInPokeball()
	{
		this.isInBall = true;
		this.unloadEntity();
	}

	public void releaseFromPokeball()
	{
		if (this.hasOwner())
		{
			this.aggression = EnumAggression.passive;
		}

		this.isDead = false;

		try
		{
			this.worldObj.spawnEntityInWorld(this);
		}
		catch (IllegalStateException arg1)
		{
			;
		}

		this.isInBall = false;
		if (!this.worldObj.isRemote && this.hasOwner())
		{
			Optional optstorage = this.getStorage();
			if (optstorage.isPresent())
			{
				((PlayerStorage) optstorage.get()).setInWorld(this, true);
			}
		}

	}

	public void clearAttackTarget()
	{
		this.setRevengeTarget((EntityLivingBase) null);
		this.setAttackTarget((EntityLivingBase) null);
	}

	public boolean getCanSpawnHere()
	{
		AxisAlignedBB aabb = this.getCollisionBoundingBox();
		MutableBlockPos pos = new MutableBlockPos();

		int wDepth;
		for (wDepth = (int) Math.floor(aabb.minX); (double) wDepth < Math.ceil(aabb.maxX); ++wDepth)
		{
			for (int y = (int) Math.floor(aabb.minY); (double) y < Math.ceil(aabb.maxY); ++y)
			{
				for (int z = (int) Math.floor(aabb.minZ); (double) z < Math.ceil(aabb.maxZ); ++z)
				{
					if (this.worldObj.getBlockState(pos.setPos(wDepth, y, z)).getMaterial().isSolid())
					{
						return false;
					}
				}
			}
		}

		if (this.getSpawnLocation() == SpawnLocation.Water)
		{
			if (this.getSwimmingParameters() == null)
			{
				this.baseStats.swimmingParameters = ((BaseStats) getBaseStats("Magikarp").get()).swimmingParameters;
			}

			wDepth = WorldHelper.getWaterDepth(this.getPosition(), this.worldObj);
			if (wDepth > this.getSwimmingParameters().depthRangeStart && wDepth < this.getSwimmingParameters().depthRangeEnd)
			{
				return true;
			}
			else
			{
				double prevPosY = this.posY;
				this.posY -= (double) (this.getSwimmingParameters().depthRangeStart + this.rand.nextInt(this.getSwimmingParameters().depthRangeEnd - this.getSwimmingParameters().depthRangeStart));
				wDepth = WorldHelper.getWaterDepth(this.getPosition(), this.worldObj);
				if (wDepth > this.getSwimmingParameters().depthRangeStart && wDepth < this.getSwimmingParameters().depthRangeEnd)
				{
					this.posY = prevPosY;
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		else
		{
			return true;
		}
	}

	public void updateLeashedState()
	{
	}

	public boolean canDespawn()
	{
		if(specialDungeonAvoidDespawn)
			return false;
		return this.legendaryTicks <= 0 && super.canDespawn();
	}
	
	public void setDead()
	{
		if(!specialDungeonAvoidDespawn)
			super.setDead();
	}

	public EntityLivingBase getOwner()
	{
		return !this.hasOwner() ? null : super.getOwner();
	}

	protected void despawnEntity()
	{
		
		if(specialDungeonAvoidDespawn)
			return;
		
		if (this.isNoDespawnRequired())
		{
			this.entityAge = 0;
		}
		else
		{
			Result result;
			if ((this.entityAge & 31) == 31 && (result = ForgeEventFactory.canEntityDespawn(this)) != Result.DEFAULT)
			{
				if (result == Result.DENY)
				{
					this.entityAge = 0;
				}
				else
				{
					this.setDead();
				}
			}
			else
			{
				EntityPlayer entityplayer = this.worldObj.getClosestPlayerToEntity(this, -1.0D);
				if (entityplayer != null)
				{
					double d0 = entityplayer.posX - this.posX;
					double d2 = entityplayer.posZ - this.posZ;
					double d3 = d0 * d0 + d2 * d2;
					if (this.canDespawn() && d3 > 16384.0D)
					{
						this.setDead();
					}

					if (d3 <= 2000.0D)
					{
						this.entityAge = 0;
					}
				}

				if (this.entityAge > 600 && this.rand.nextInt(500) == 0 && this.canDespawn())
				{
					this.setDead();
				}
			}
		}

	}

	public void onUpdate()
	{
		try
		{
			if (Pixelmon.freeze)
			{
				return;
			}

			if (Pixelmon.EVENT_BUS.post(new PixelmonUpdateEvent(this, Phase.START)))
			{
				return;
			}

			if (this.posY < 0.0D && !this.worldObj.isRemote)
			{
				if (this.battleController != null)
				{
					this.battleController.endBattleWithoutXP();
				}

				this.setDead();
			}

			if ((this.hasNPCTrainer || this.trainer != null) && this.battleController == null)
			{
				this.setDead();
			}

			if (this.canDespawn && !this.worldObj.isRemote)
			{
				this.checkForRarityDespawn();
				if (this.legendaryTicks >= 0 && this.battleController == null)
				{
					--this.legendaryTicks;
					if (this.legendaryTicks == 0)
					{
						this.setDead();
					}
				}
			}

			if (this.playerOwned && this.getOwner() == null)
			{
				this.setDead();
			}

			super.onUpdate();
			Pixelmon.EVENT_BUS.post(new PixelmonUpdateEvent(this, Phase.END));
		}
		catch (Exception arg1)
		{
			Pixelmon.LOGGER.error("Error in ticking Pixelmon entity.");
			arg1.printStackTrace();
		}

	}

	private void checkForRarityDespawn()
	{
		if (this.legendaryTicks <= 0 && (this.battleController == null || !this.battleController.containsParticipantType(PlayerParticipant.class)) && this.getOwner() == null && this.baseStats != null && this.blockOwner == null)
		{
			if (this.despawnCounter > 0)
			{
				--this.despawnCounter;
			}
			else if (this.despawnCounter == 0)
			{
				if (!this.playersNearby())
				{
					if (this.battleController != null)
					{
						this.battleController.endBattleWithoutXP();
					}

					this.setDead();
				}
			}
			else
			{
				this.despawnCounter = (int) (Math.random() * (double) (intMaxTicksToDespawn - intMinTicksToDespawn) + (double) intMinTicksToDespawn);
			}

		}
	}

	private boolean playersNearby()
	{
		for (int i = 0; i < this.worldObj.playerEntities.size(); ++i)
		{
			EntityPlayer player = (EntityPlayer) this.worldObj.playerEntities.get(i);
			double distancex = player.posX - this.posX;
			double distancey = player.posY - this.posY;
			double distancez = player.posZ - this.posZ;
			double distancesquared = distancex * distancex + distancey * distancey + distancez * distancez;
			if (distancesquared < (double) (despawnRadius * despawnRadius))
			{
				return true;
			}
		}

		return false;
	}

	public void writeEntityToNBT(NBTTagCompound nbt)
	{
		super.writeEntityToNBT(nbt);
		if (this.getOwner() != null)
		{
			nbt.setUniqueId("pixelmonOwnerUUID", this.getOwnerId());
		}

		if (this.getSpawnLocation() == null)
		{
			this.setSpawnLocation(SpawnLocation.Land);
		}

		nbt.setInteger("pixelmonType", this.getSpawnLocation().ordinal());
		if (this.legendaryTicks > 0)
		{
			nbt.setInteger("legendaryTicks", this.legendaryTicks);
			nbt.setLong("legendaryTime", this.worldObj.getTotalWorldTime());
		}

	}

	public void getNBTTags(HashMap<String, Class> tags)
	{
		super.getNBTTags(tags);
		tags.put("pixelmonOwnerUUID", String.class);
		tags.put("pixelmonType", Integer.class);
	}

	public void readEntityFromNBT(NBTTagCompound nbt)
	{
		try
		{
			super.readEntityFromNBT(nbt);
			if (nbt.hasKey("pixelmonOwnerUUID"))
			{
				this.setOwnerId(nbt.getUniqueId("pixelmonOwnerUUID"));
			}

			float e = this.getHealth();
			this.level.readFromNBT(nbt);
			this.setHealth(e);
			if (nbt.hasKey("pixelmonType"))
			{
				this.setSpawnLocation(SpawnLocation.getFromIndex(nbt.getInteger("pixelmonType")));
			}
			else if (this.baseStats.spawnLocations[0] == SpawnLocation.Land)
			{
				this.setSpawnLocation(SpawnLocation.Land);
			}
			else
			{
				this.setSpawnLocation(SpawnLocation.Water);
			}

			this.aiHelper = new AIHelper(this.getPokemonName(), this, this.tasks);
			if (nbt.hasKey("legendaryTicks"))
			{
				this.legendaryTicks = nbt.getInteger("legendaryTicks");
				long lastTime = nbt.getLong("legendaryTime");
				this.legendaryTicks = (int) ((long) this.legendaryTicks - (this.worldObj.getTotalWorldTime() - lastTime));
				if (this.legendaryTicks <= 0)
				{
					this.setDead();
				}
			}
		}
		catch (Exception arg4)
		{
			arg4.printStackTrace();
		}

	}

	public void unloadEntity()
	{
		super.unloadEntity();
		this.worldObj.removeEntity(this);
		this.clearAttackTarget();
	}

	public EntityAgeable createChild(EntityAgeable var1)
	{
		return null;
	}

	public EnumPokemon[] getPreEvolutions()
	{
		return this.baseStats.preEvolutions;
	}

	public boolean canBeLeashedTo(EntityPlayer player)
	{
		return this.getOwner() == player;
	}

	public void startEvolution(String evolutionName, boolean fromLevelUp)
	{
		Optional optstorage = this.getStorage();
		if (optstorage.isPresent())
		{
			((PlayerStorage) optstorage.get()).guiOpened = true;
			new EvolutionQuery(this, evolutionName, fromLevelUp);
		}

	}

	public boolean isLoaded()
	{
		return this.isLoaded(false);
	}

	public boolean isLoaded(boolean checkChunk)
	{
		boolean isLoaded = true;
		if (checkChunk)
		{
			isLoaded = this.worldObj.isAreaLoaded(this.getPosition(), 1);
		}

		if (isLoaded)
		{
			isLoaded = this.worldObj.getEntityByID(this.getEntityId()) != null;
		}

		return isLoaded;
	}

	public boolean checkEvolution(EvolutionType... typeList)
	{
		Evolution[] arg1 = this.baseStats.evolutions;
		int arg2 = arg1.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			Evolution e = arg1[arg3];
			EvolutionType[] arg5 = typeList;
			int arg6 = typeList.length;

			for (int arg7 = 0; arg7 < arg6; ++arg7)
			{
				EvolutionType type = arg5[arg7];
				if (e.handleEvolution(this, type))
				{
					return true;
				}
			}
		}

		return false;
	}

	public void setSpawnerParent(TileEntityPixelmonSpawner spawner)
	{
		this.spawner = spawner;
	}

	public boolean isInRangeToRender3d(double p_145770_1_, double p_145770_3_, double p_145770_5_)
	{
		double d3;
		double d4;
		double d5;
		if (!this.onGround && this.posY > 64.0D)
		{
			d3 = this.posX - p_145770_1_;
			d4 = this.posZ - p_145770_5_;
			d5 = d3 * d3 + d4 * d4;
			return this.isInRangeToRenderDist(d5);
		}
		else
		{
			d3 = this.posX - p_145770_1_;
			d4 = this.posY - p_145770_3_;
			d5 = this.posZ - p_145770_5_;
			double d6 = d3 * d3 + d4 * d4 + d5 * d5;
			return this.isInRangeToRenderDist(d6);
		}
	}

	public void playPixelmonSound()
	{
		if (this.baseStats.hasSoundForGender(this.gender))
		{
			this.playLivingSound();
		}

	}

	public String getNickname()
	{
		if (this.getBattleAbility() instanceof Illusion)
		{
			Illusion illusion = (Illusion) this.getBattleAbility();
			if (illusion.disguisedPokemon != null)
			{
				if (!illusion.disguisedNickname.equals(""))
				{
					return illusion.disguisedNickname;
				}

				return getLocalizedName(illusion.disguisedPokemon.name);
			}
		}

		return super.getNickname();
	}

	public String getRealNickname()
	{
		return super.getNickname();
	}

	public boolean isPokemon(String... matches)
	{
		String[] arg1 = matches;
		int arg2 = matches.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			String match = arg1[arg3];
			if (this.getPokemonName().equalsIgnoreCase(match))
			{
				return true;
			}
		}

		return false;
	}

	public boolean isPokemon(EnumPokemon... pokemon)
	{
		EnumPokemon[] arg1 = pokemon;
		int arg2 = pokemon.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			EnumPokemon p = arg1[arg3];
			if (this.baseStats.pokemon == p)
			{
				return true;
			}
		}

		return false;
	}

	static
	{
		dwName = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
		dwNickname = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
		dwPokemonID1 = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwPokemonID2 = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwExp = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwTrainerName = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
		dwTextures = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwTextureVariation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BYTE);
		dwScale = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwMaxHP = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwBossMode = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwNature = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwGrowth = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwNumInteractions = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwForm = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwLevel = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwTransformation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwSpawnLocation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		dwNumBreedingLevels = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
		despawnRadius = 20;
		TICKSPERSECOND = 20;
		intMinTicksToDespawn = 30 * TICKSPERSECOND;
		intMaxTicksToDespawn = 180 * TICKSPERSECOND;
		interactionList = new ArrayList();
	}
}