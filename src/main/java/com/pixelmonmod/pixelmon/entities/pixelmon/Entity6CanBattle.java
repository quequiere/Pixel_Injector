/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.entities.pixelmon;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.events.PixelmonAttackEvent;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.attacks.BattleDamageSource;
import com.pixelmonmod.pixelmon.battles.attacks.DamageTypeEnum;
import com.pixelmonmod.pixelmon.battles.attacks.EffectBase;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.attackModifiers.MultipleHit;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.attackModifiers.Recoil;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.BeatUp;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.TripleKick;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.log.AttackResult;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.ParticipantType;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.battles.status.GlobalStatusBase;
import com.pixelmonmod.pixelmon.battles.status.StatusBase;
import com.pixelmonmod.pixelmon.battles.status.StatusPersist;
import com.pixelmonmod.pixelmon.battles.status.StatusType;
import com.pixelmonmod.pixelmon.battles.status.Substitute;
import com.pixelmonmod.pixelmon.battles.status.TempMoveset;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.comm.packetHandlers.battles.gui.StatusPacket;
import com.pixelmonmod.pixelmon.database.DatabaseMoves;
import com.pixelmonmod.pixelmon.entities.npcs.NPCTrainer;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity5Rideable;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.AbilityBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.ComingSoon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.BattleStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import com.pixelmonmod.pixelmon.enums.EnumBossMode;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.enums.battle.EnumBattleType;
import com.pixelmonmod.pixelmon.enums.forms.EnumBurmy;
import com.pixelmonmod.pixelmon.items.ItemHeld;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public abstract class Entity6CanBattle extends Entity5Rideable
{
	public BattleStats battleStats = new BattleStats(this);
	private ArrayList<StatusBase> status = new ArrayList();
	private Moveset moveset = new Moveset();
	public BattleControllerBase battleController;
	protected NPCTrainer trainer;
	private PixelmonWrapper pixelmonWrapper;
	public boolean hasNPCTrainer;

	public Entity6CanBattle(World par1World)
	{
		super(par1World);
		this.dataManager.register(EntityPixelmon.dwTrainerName, "");
	}

	public boolean canDespawn()
	{
		return this.battleController != null ? false : super.canDespawn();
	}

	public void loadMoveset()
	{
		this.moveset = DatabaseMoves.GetInitialMoves(this, this.getLvl().getLevel());
	}

	public void loadMoveset(String... moves)
	{
		this.loadMoveset();
		int j = 0;
		String[] arg2 = moves;
		int arg3 = moves.length;

		for (int arg4 = 0; arg4 < arg3; ++arg4)
		{
			String move = arg2[arg4];
			Attack attack = DatabaseMoves.getAttack(move);
			if (!this.moveset.add(attack))
			{
				this.moveset.set(j++, attack);
			}
		}

	}

	public void setMoveset(Moveset moveset)
	{
		this.moveset = moveset;
	}

	public Moveset getMoveset()
	{
		for (int i = 0; i < this.status.size(); ++i)
		{
			if (this.getStatus(i) instanceof TempMoveset)
			{
				return ((TempMoveset) this.getStatus(i)).getMoveset();
			}
		}

		return this.moveset;
	}

	public void StartBattle(BattleParticipant p1, BattleParticipant p2)
	{
		this.StartBattle(p1, p2, EnumBattleType.Single);
	}

	public void StartBattle(BattleParticipant p1, BattleParticipant p2, EnumBattleType battleType)
	{
		if (this.moveset.isEmpty())
		{
			this.loadMoveset();
		}

		try
		{
			p1.startedBattle = true;
			this.battleController = new BattleControllerBase(p1, p2, battleType);
		}
		catch (Exception arg4)
		{
			arg4.printStackTrace();
		}

	}

	public void endBattle()
	{
		if (this.baseStats.pokemon == EnumPokemon.Burmy)
		{
			this.setForm(EnumBurmy.getFromType(TerrainExamine.getTerrain(this.pixelmonWrapper)).ordinal());
		}

		this.battleController = null;
		this.pixelmonWrapper = null;
	}

	public void setTrainer(NPCTrainer trainer)
	{
		this.trainer = trainer;
		this.dataManager.set(EntityPixelmon.dwTrainerName, trainer.getName());
	}

	public NPCTrainer getTrainer()
	{
		return this.trainer;
	}

	public String getTrainerName()
	{
		return (String) this.dataManager.get(EntityPixelmon.dwTrainerName);
	}

	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
	{
		if (!this.worldObj.isRemote)
		{
			if (this.getBossMode() != EnumBossMode.NotBoss && par1DamageSource.damageType.equals("mob") && !(par1DamageSource.getEntity() instanceof EntityPixelmon))
			{
				return false;
			}
			else if (!par1DamageSource.damageType.equals("player") && par1DamageSource != DamageSource.cactus && !par1DamageSource.damageType.equals("arrow"))
			{
				if (this.battleController == null || par1DamageSource != DamageSource.cactus && par1DamageSource != DamageSource.drown && par1DamageSource != DamageSource.fall && par1DamageSource != DamageSource.inFire && par1DamageSource != DamageSource.inWall && par1DamageSource != DamageSource.lava && par1DamageSource != DamageSource.onFire && par1DamageSource != DamageSource.fallingBlock && par1DamageSource != DamageSource.anvil)
				{
					boolean flag = super.attackEntityFrom(par1DamageSource, par2);
					this.updateHealth();
					EntityPixelmon thisPokemon = (EntityPixelmon) this;
					if (this.battleController != null)
					{
						if (!(par1DamageSource instanceof BattleDamageSource))
						{
							return false;
						}

						this.battleController.updatePokemonHealth(thisPokemon);
						if (this.battleController != null)
						{
							this.battleController.sendDamagePacket(thisPokemon, (int) par2);
						}
					}

					if (this.getHealth() <= 0.0F)
					{
						if (this.battleController != null)
						{
							PixelmonWrapper entity = thisPokemon.getPixelmonWrapper();
							int turnIndex = this.battleController.turnList.indexOf(entity);
							if (turnIndex > this.battleController.turn)
							{
								this.battleController.turnList.remove(turnIndex);
							}

							if (this.removePrimaryStatus() != null)
							{
								this.sendStatusPacket(-1);
								this.update(new EnumUpdateType[] { EnumUpdateType.Status });
							}
						}

						this.onDeath(par1DamageSource);
					}

					Entity entity1 = par1DamageSource.getEntity();
					if (this.getOwner() != null)
					{
						this.update(new EnumUpdateType[] { EnumUpdateType.HP });
					}

					if (this.isValidTarget(entity1))
					{
						this.setAttackTarget((EntityLiving) entity1);
					}

					return flag;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public ArrayList<Attack> getAttacksAtLevel(int level)
	{
		return DatabaseMoves.getAttacksAtLevel(this.baseStats.baseFormID, level);
	}

	public boolean LearnsAttackAtLevel(int level)
	{
		return DatabaseMoves.LearnsAttackAtLevel(this.baseStats.baseFormID, level);
	}

	protected boolean isValidTarget(Entity entity)
	{
		return entity instanceof EntityPixelmon;
	}

	public boolean removeStatus(StatusType s)
	{
		for (int i = 0; i < this.status.size(); ++i)
		{
			StatusBase base = (StatusBase) this.status.get(i);
			if (base.type == s)
			{
				this.removeStatus(base);
				return true;
			}
		}

		return false;
	}

	public boolean removeStatuses(StatusType... statuses)
	{
		boolean wasRemoved = false;

		for (int i = 0; i < this.status.size(); ++i)
		{
			StatusBase base = (StatusBase) this.status.get(i);
			StatusType[] arg4 = statuses;
			int arg5 = statuses.length;

			for (int arg6 = 0; arg6 < arg5; ++arg6)
			{
				StatusType type = arg4[arg6];
				if (base.type == type)
				{
					int beforeSize = this.status.size();
					this.removeStatus(base);
					if (this.status.size() < beforeSize)
					{
						--i;
					}

					wasRemoved = true;
					break;
				}
			}
		}

		return wasRemoved;
	}

	public void removeStatus(int i)
	{
		this.removeStatus((StatusBase) this.status.get(i));
	}

	public StatusBase getStatus(StatusType type)
	{
		try
		{
			Iterator e = this.status.iterator();

			while (e.hasNext())
			{
				StatusBase base = (StatusBase) e.next();
				if (base.type == type)
				{
					return base;
				}
			}
		}
		catch (Exception arg3)
		{
			arg3.printStackTrace();
		}

		return null;
	}

	public boolean hasStatus(StatusType... statuses)
	{
		try
		{
			Iterator e = this.status.iterator();

			while (e.hasNext())
			{
				StatusBase base = (StatusBase) e.next();
				StatusType[] arg3 = statuses;
				int arg4 = statuses.length;

				for (int arg5 = 0; arg5 < arg4; ++arg5)
				{
					StatusType current = arg3[arg5];
					if (base.type == current)
					{
						return true;
					}
				}
			}

			return false;
		}
		catch (Exception arg7)
		{
			arg7.printStackTrace();
			return false;
		}
	}

	public boolean hasPrimaryStatus()
	{
		return this.hasStatus(new StatusType[] { StatusType.Poison, StatusType.Burn, StatusType.PoisonBadly, StatusType.Freeze, StatusType.Sleep, StatusType.Paralysis });
	}

	public int countStatuses(StatusType... statuses)
	{
		int count = 0;
		Iterator arg2 = this.status.iterator();

		while (arg2.hasNext())
		{
			StatusBase base = (StatusBase) arg2.next();
			StatusType[] arg4 = statuses;
			int arg5 = statuses.length;

			for (int arg6 = 0; arg6 < arg5; ++arg6)
			{
				StatusType current = arg4[arg6];
				if (base.type == current)
				{
					++count;
				}
			}
		}

		return count;
	}

	public void writeEntityToNBT(NBTTagCompound nbt)
	{
		super.writeEntityToNBT(nbt);
		this.moveset.writeToNBT(nbt);
		if (nbt.hasKey("StatusCount"))
		{
			short status = nbt.getShort("StatusCount");

			for (int i = 0; i < status; ++i)
			{
				nbt.removeTag("Status" + i);
			}

			nbt.removeTag("StatusCount");
		}

		StatusPersist arg3 = (StatusPersist) this.getPrimaryStatus();
		if (arg3 == null)
		{
			nbt.removeTag("Status");
		}
		else
		{
			arg3.writeToNBT(nbt);
		}

		if (this.trainer != null)
		{
			nbt.setBoolean("HasNPCTrainer", true);
		}

	}

	public void getNBTTags(HashMap<String, Class> tags)
	{
		super.getNBTTags(tags);
		this.moveset.getNBTTags(tags);
		tags.put("Status", Integer.class);
		tags.put("StatusSleepTurns", Integer.class);
		tags.put("HasNPCTrainer", Boolean.class);
	}

	public void readEntityFromNBT(NBTTagCompound nbt)
	{
		super.readEntityFromNBT(nbt);
		this.moveset.readFromNBT(nbt);
		boolean statusCount = false;
		if (nbt.hasKey("Status"))
		{
			StatusPersist i = StatusType.getEffectInstance(nbt.getInteger("Status"));
			if (i != null)
			{
				this.status.add(i.restoreFromNBT(nbt));
			}
		}
		else if (nbt.hasKey("StatusCount"))
		{
			short arg4 = nbt.getShort("StatusCount");

			for (int arg5 = 0; arg5 < arg4; ++arg5)
			{
				StatusPersist s = StatusType.getEffectInstance(nbt.getInteger("Status" + arg5));
				if (s != null)
				{
					this.status.add(s.restoreFromNBT(nbt));
					break;
				}
			}
		}

		if (nbt.hasKey("HasNPCTrainer"))
		{
			this.hasNPCTrainer = nbt.getBoolean("HasNPCTrainer");
		}

	}

	public void setHealth(float par1)
	{
		super.setHealth(par1);
		if (this.battleController != null)
		{
			this.battleController.updatePokemonHealth((EntityPixelmon) this);
		}

	}

	public BattleParticipant getParticipant()
	{
		if (this.battleController == null)
		{
			return null;
		}
		else
		{
			Iterator arg0 = this.battleController.participants.iterator();

			while (arg0.hasNext())
			{
				BattleParticipant p = (BattleParticipant) arg0.next();
				PixelmonWrapper[] arg2 = p.controlledPokemon;
				int arg3 = arg2.length;

				for (int arg4 = 0; arg4 < arg3; ++arg4)
				{
					PixelmonWrapper pw = arg2[arg4];
					if (pw.pokemon == this)
					{
						return p;
					}
				}
			}

			return null;
		}
	}

	public int getStatusSize()
	{
		return this.status.size();
	}

	public ArrayList<StatusBase> getStatuses()
	{
		return this.status;
	}

	public StatusBase getStatus(int i)
	{
		return (StatusBase) this.status.get(i);
	}

	public int getStatusIndex(StatusType findStatus)
	{
		try
		{
			for (int e = 0; e < this.status.size(); ++e)
			{
				StatusBase base = (StatusBase) this.status.get(e);
				if (base.type == findStatus)
				{
					return e;
				}
			}

			return -1;
		}
		catch (Exception arg3)
		{
			return -1;
		}
	}

	public StatusBase getPrimaryStatus()
	{
		Iterator arg0 = this.status.iterator();

		StatusBase s;
		do
		{
			if (!arg0.hasNext())
			{
				return null;
			}

			s = (StatusBase) arg0.next();
		}
		while (!(s instanceof StatusPersist));

		return s;
	}

	public void setStatus(int i, StatusBase newStatus)
	{
		this.status.set(i, newStatus);
	}

	public void clearStatus()
	{
		if (this.hasPrimaryStatus())
		{
			this.sendStatusPacket(-1);
		}

		this.status.clear();
	}

	public boolean addStatus(StatusBase e, EntityPixelmon opponent)
	{
		return this.addStatus(e, opponent, (TextComponentTranslation) null);
	}

	public boolean addStatus(StatusBase e, EntityPixelmon opponent, TextComponentTranslation message)
	{
		EntityPixelmon thisPokemon = (EntityPixelmon) this;
		if (this.cannotHaveStatus(e, thisPokemon, opponent))
		{
			return false;
		}
		else if (this.battleController == null)
		{
			if (e.type.isPrimaryStatus())
			{
				this.status.add(e);
				if (this.getOwner() != null)
				{
					this.update(new EnumUpdateType[] { EnumUpdateType.Status });
				}
			}

			return true;
		}
		else
		{
			e.applyBeforeEffect(thisPokemon, opponent);
			if (!this.battleController.simulateMode)
			{
				this.status.add(e);
			}

			if (this.battleController.sendMessages)
			{
				if (message != null)
				{
					this.battleController.sendToAll(message);
				}

				if (!this.battleController.simulateMode && e.type.isPrimaryStatus())
				{
					this.sendStatusPacket(e.type.ordinal());
				}

				thisPokemon.getBattleAbility().onStatusAdded(e, thisPokemon, opponent);
				if (ItemHeld.canUseItem(thisPokemon))
				{
					thisPokemon.getItemHeld().onStatusAdded(thisPokemon, opponent, e);
				}

				if (!this.battleController.simulateMode)
				{
					if (this.getOwner() != null)
					{
						this.update(new EnumUpdateType[] { EnumUpdateType.Status });
					}

					this.battleController.updatePokemonHealth(thisPokemon);
				}
			}

			return true;
		}
	}

	public void removeStatus(StatusBase e)
	{
		if (!this.battleController.simulateMode)
		{
			EntityPixelmon thisPokemon = (EntityPixelmon) this;
			this.status.remove(e);
			if (StatusType.isPrimaryStatus(e.type))
			{
				this.sendStatusPacket(-1);
			}

			thisPokemon.update(new EnumUpdateType[] { EnumUpdateType.Status });
			if (this.battleController != null)
			{
				this.battleController.updatePokemonHealth(thisPokemon);
			}

		}
	}

	public StatusBase removePrimaryStatus()
	{
		try
		{
			for (int e = 0; e < this.status.size(); ++e)
			{
				StatusBase base = (StatusBase) this.status.get(e);
				if (StatusType.isPrimaryStatus(base.type))
				{
					this.removeStatus(base);
					return base;
				}
			}

			return null;
		}
		catch (Exception arg2)
		{
			arg2.printStackTrace();
			return null;
		}
	}

	public void sendStatusPacket(int statusID)
	{
		if (this.battleController != null)
		{
			int[] pokemonID = this.getPokemonId();
			this.battleController.participants.stream().filter((p) -> {
				return p.getType() == ParticipantType.Player;
			}).forEach((p) -> {
				Pixelmon.network.sendTo(new StatusPacket(pokemonID, statusID), ((PlayerParticipant) p).player);
			});
			this.battleController.spectators.forEach((spectator) -> {
				spectator.sendMessage(new StatusPacket(pokemonID, statusID));
			});
		}

	}

	public boolean cannotHaveStatus(StatusBase t, EntityPixelmon pokemon, EntityPixelmon opponent)
	{
		return this.cannotHaveStatus(t, pokemon, opponent, false);
	}

	public boolean cannotHaveStatus(StatusBase t, EntityPixelmon pokemon, EntityPixelmon opponent, boolean ignorePrimaryOverlap)
	{
		StatusType type = t.type;
		if (StatusType.isPrimaryStatus(type) && pokemon.hasPrimaryStatus() && !ignorePrimaryOverlap)
		{
			return true;
		}
		else if (!t.isImmune(pokemon) && pokemon.battleController != null && (pokemon.getHealth() > 0.0F || t.isTeamStatus()) && !pokemon.hasStatus(new StatusType[] { type }))
		{
			Iterator arg5 = this.status.iterator();

			while (arg5.hasNext())
			{
				StatusBase ally = (StatusBase) arg5.next();

				try
				{
					if (ally.stopsStatusChange(type, pokemon, opponent))
					{
						return true;
					}
				}
				catch (Exception arg9)
				{
					System.out.println("Problem with cannotHaveStatus for StatusType " + type.name());
					arg9.printStackTrace();
				}
			}

			arg5 = pokemon.battleController.globalStatusController.getGlobalStatuses().iterator();

			while (arg5.hasNext())
			{
				GlobalStatusBase ally1 = (GlobalStatusBase) arg5.next();

				try
				{
					if (ally1.stopsStatusChange(type, pokemon, opponent))
					{
						return true;
					}
				}
				catch (Exception arg8)
				{
					System.out.println("Problem with cannotHaveStatus for StatusType " + type.name());
					arg8.printStackTrace();
				}
			}

			arg5 = pokemon.battleController.getTeamPokemon(pokemon).iterator();

			PixelmonWrapper ally2;
			do
			{
				if (!arg5.hasNext())
				{
					return !pokemon.getBattleAbility().allowsStatus(type, pokemon, opponent);
				}

				ally2 = (PixelmonWrapper) arg5.next();
			}
			while (ally2.pokemon.getBattleAbility().allowsStatusTeammate(type, ally2.pokemon, pokemon, opponent));

			return true;
		}
		else
		{
			return true;
		}
	}

	public ArrayList<EnumType> getEffectiveTypes(EntityPixelmon user, EntityPixelmon target)
	{
		ArrayList effectiveTypes = target.type;
		effectiveTypes = user.getBattleAbility().getEffectiveTypes(user, target);
		if (effectiveTypes != target.type)
		{
			return effectiveTypes;
		}
		else
		{
			for (int i = 0; i < target.getStatusSize(); ++i)
			{
				effectiveTypes = target.getStatus(i).getEffectiveTypes(user, target);
				if (effectiveTypes != target.type)
				{
					return effectiveTypes;
				}
			}

			if (ItemHeld.canUseItem(target))
			{
				effectiveTypes = target.getItemHeld().getEffectiveTypes(user, target);
			}

			GlobalStatusBase status;
			if (target.battleController != null)
			{
				for (Iterator arg5 = target.battleController.globalStatusController.getGlobalStatuses().iterator(); arg5.hasNext(); effectiveTypes = status.getEffectiveTypes(user, target))
				{
					status = (GlobalStatusBase) arg5.next();
				}
			}

			return effectiveTypes;
		}
	}

	public float doBattleDamage(PixelmonWrapper source, float damage, DamageTypeEnum damageType)
	{
		//quequiere
		PixelmonAttackEvent attackevent = new PixelmonAttackEvent(this,source, damage, damageType);
		Pixelmon.EVENT_BUS.post(attackevent);
		damage=attackevent.getDamage();
		
		EntityPixelmon thisPokemon = (EntityPixelmon) this;
		PixelmonWrapper thisWrapper = thisPokemon.getPixelmonWrapper();
		if (thisWrapper == null)
		{
			return 0.0F;
		}
		else
		{
			if (thisWrapper.bc == null)
			{
				if (this.battleController == null)
				{
					return 0.0F;
				}

				thisWrapper.bc = this.battleController;
			}

			AbilityBase thisAbility = this.getBattleAbility();
			boolean thisCanUseItem = ItemHeld.canUseItem(thisPokemon);
			ItemHeld thisHeldItem = thisPokemon.getItemHeld();
			if (source == null)
			{
				source = thisWrapper;
			}

			if (this.getHealth() <= 0.0F)
			{
				return -1.0F;
			}
			else
			{
				boolean isMultiHit = false;
				Iterator arg14;
				if (source.pokemon != thisPokemon)
				{
					Iterator damageResult;
					if (damageType == DamageTypeEnum.ATTACK)
					{
						damageResult = source.attack.baseAttack.effects.iterator();

						label177:
						{
							EffectBase name;
							do
							{
								if (!damageResult.hasNext())
								{
									break label177;
								}

								name = (EffectBase) damageResult.next();
							}
							while (!(name instanceof MultipleHit) && !(name instanceof TripleKick) && !(name instanceof BeatUp));

							isMultiHit = true;
						}

						if (!isMultiHit)
						{
							source.attack.sendEffectiveChat(source, thisWrapper);
						}

						ArrayList arg12 = thisWrapper.bc.getTeamPokemon(thisWrapper.getParticipant());
						if (arg12.size() > 1)
						{
							arg14 = arg12.iterator();

							while (arg14.hasNext())
							{
								PixelmonWrapper effect = (PixelmonWrapper) arg14.next();
								if (effect != thisWrapper)
								{
									damage = (float) effect.pokemon.getBattleAbility().modifyDamageTeammate((int) damage, source.pokemon, thisPokemon, source.attack);
								}
							}
						}

						damage = (float) source.pokemon.getBattleAbility().modifyDamageUser((int) damage, source.pokemon, thisPokemon, source.attack);
						if (!AbilityBase.ignoreAbility(source.pokemon, thisPokemon))
						{
							damage = (float) thisAbility.modifyDamageTarget((int) damage, source.pokemon, thisPokemon, source.attack);
							damage = (float) thisAbility.modifyDamageIncludeFixed((int) damage, source.pokemon, thisPokemon, source.attack);
						}

						StatusBase arg17;
						for (arg14 = this.getStatuses().iterator(); arg14.hasNext(); damage = (float) arg17.modifyDamageIncludeFixed((int) damage, source.pokemon, thisPokemon, source.attack))
						{
							arg17 = (StatusBase) arg14.next();
						}

						if (thisCanUseItem)
						{
							damage = (float) thisHeldItem.modifyDamageIncludeFixed((double) damage, source.pokemon, thisPokemon, source.attack);
						}
					}
					else if (damageType == DamageTypeEnum.ATTACKFIXED)
					{
						StatusBase arg15;
						for (damageResult = this.getStatuses().iterator(); damageResult.hasNext(); damage = (float) arg15.modifyDamageIncludeFixed((int) damage, source.pokemon, thisPokemon, source.attack))
						{
							arg15 = (StatusBase) damageResult.next();
						}

						damage = (float) thisAbility.modifyDamageIncludeFixed((int) damage, source.pokemon, thisPokemon, source.attack);
						if (thisCanUseItem)
						{
							damage = (float) thisHeldItem.modifyDamageIncludeFixed((double) damage, source.pokemon, thisPokemon, source.attack);
						}
					}
				}

				damage = (float) Math.floor((double) damage);
				float arg13;
				if ((damageType == DamageTypeEnum.ATTACK || damageType == DamageTypeEnum.ATTACKFIXED) && source.pokemon != this)
				{
					Substitute arg16 = (Substitute) this.getStatus(StatusType.Substitute);
					if (arg16 != null && !arg16.ignoreSubstitute(source))
					{
						arg13 = arg16.attackSubstitute(damage, thisPokemon);
						if (ItemHeld.canUseItem(source.pokemon))
						{
							source.pokemon.getItemHeld().postProcessAttackUser(source, thisWrapper, source.attack, arg13);
						}

						source.attack.moveResult.damage = (int) arg13;
						source.attack.moveResult.fullDamage = (int) damage;
						source.attack.moveResult.target = thisWrapper;
						source.attack.moveResult.result = AttackResult.succeeded;
						return arg13;
					}
				}

				if (source.attack != null && source.attack.baseAttack.isAttack("False Swipe"))
				{
					damage = Math.min(damage, this.getHealth() - 1.0F);
				}

				arg13 = Math.min(thisPokemon.getHealth(), damage);
				if (source != thisWrapper && source.attack != null && source.attack.moveResult != null)
				{
					source.attack.moveResult.damage = (int) arg13;
					source.attack.moveResult.fullDamage = (int) damage;
					source.attack.moveResult.target = thisWrapper;
					source.attack.moveResult.result = AttackResult.hit;
				}

				if (!this.battleController.simulateMode)
				{
					this.hurtResistantTime = 0;
					this.attackEntityFrom(new BattleDamageSource("battle", source), damage);
					NPCTrainer arg18 = this.getTrainer();
					if (arg18 != null)
					{
						arg18.getPokemonStorage().updateNBT(thisPokemon);
					}
				}

				for (int arg20 = 0; arg20 < this.getStatusSize(); ++arg20)
				{
					this.getStatus(arg20).onDamageReceived(source, thisPokemon, source.attack, (int) arg13, damageType);
				}

				if (source.pokemon != thisPokemon && damageType.isDirect())
				{
					if (source.attack.baseAttack.getMakesContact())
					{
						Attack.applyContact(source.pokemon, thisPokemon);
					}

					source.pokemon.getBattleAbility().tookDamageUser((int) arg13, source.pokemon, thisPokemon, source.attack);
					thisAbility.tookDamageTarget((int) arg13, source.pokemon, thisPokemon, source.attack);
					if (ItemHeld.canUseItem(source.pokemon))
					{
						source.pokemon.getItemHeld().postProcessAttackUser(source, thisWrapper, source.attack, arg13);
					}

					if (!isMultiHit)
					{
						Attack.postProcessAttackAllHits(source, thisWrapper, source.attack, arg13, damageType);
					}

					arg14 = source.attack.baseAttack.effects.iterator();

					while (arg14.hasNext())
					{
						EffectBase arg19 = (EffectBase) arg14.next();
						if (arg19 instanceof Recoil)
						{
							((Recoil) arg19).applyRecoil(source, arg13);
						}
					}
				}

				if (thisCanUseItem && (source.attack == null || !source.attack.canRemoveBerry()))
				{
					thisHeldItem.tookDamage(source.pokemon, thisPokemon, arg13, damageType);
				}

				if (this.getHealth() <= 0.0F)
				{
					if (thisWrapper.bc != null)
					{
						String arg21 = this.getNickname();
						thisWrapper.bc.sendToAll("battlecontroller.hasfainted", new Object[] { arg21 });
					}

					this.isFainted = true;
				}
				else if (!thisWrapper.bc.simulateMode)
				{
					thisWrapper.damageTakenThisTurn = (int) ((float) thisWrapper.damageTakenThisTurn + damage);
				}

				return arg13;
			}
		}
	}

	public AbilityBase getBattleAbility()
	{
		return this.getBattleAbility(true, (EntityPixelmon) null);
	}

	public AbilityBase getBattleAbility(EntityPixelmon moveUser)
	{
		return this.getBattleAbility(true, moveUser);
	}

	public AbilityBase getBattleAbility(boolean canIgnore)
	{
		return this.getBattleAbility(canIgnore, (EntityPixelmon) null);
	}

	public AbilityBase getBattleAbility(boolean canIgnore, EntityPixelmon moveUser)
	{
		EntityPixelmon thisPokemon = (EntityPixelmon) this;
		PixelmonWrapper thisWrapper = thisPokemon.getPixelmonWrapper();
		return (AbilityBase) (canIgnore && AbilityBase.ignoreAbility(moveUser, thisPokemon) ? ComingSoon.noAbility : (thisWrapper != null && thisWrapper.tempAbility != null ? thisWrapper.tempAbility : this.getAbility()));
	}

	public PixelmonWrapper getPixelmonWrapper()
	{
		if (this.battleController == null)
		{
			return null;
		}
		else if (this.pixelmonWrapper != null && this.pixelmonWrapper.bc != null)
		{
			return this.pixelmonWrapper;
		}
		else
		{
			Iterator arg0 = this.battleController.participants.iterator();

			while (true)
			{
				BattleParticipant bp;
				do
				{
					if (!arg0.hasNext())
					{
						return null;
					}

					bp = (BattleParticipant) arg0.next();
				}
				while (bp.controlledPokemon == null);

				PixelmonWrapper[] arg2 = bp.controlledPokemon;
				int arg3 = arg2.length;

				for (int arg4 = 0; arg4 < arg3; ++arg4)
				{
					PixelmonWrapper pw = arg2[arg4];
					if (pw != null && pw.pokemon == this)
					{
						this.pixelmonWrapper = pw;
						return pw;
					}
				}
			}
		}
	}

	public void setPixelmonWrapper(PixelmonWrapper newWrapper)
	{
		this.pixelmonWrapper = newWrapper;
	}

	public float getHealthPercent()
	{
		return this.getHealthPercent(this.getHealth());
	}

	public float getHealthPercent(float amount)
	{
		return amount / this.getMaxHealth() * 100.0F;
	}

	public float getHealPercent(float amount)
	{
		return this.getHealthPercent(Math.min(amount, this.getHealthDeficit()));
	}

	public float getHealthDeficit()
	{
		return this.getMaxHealth() - this.getHealth();
	}

	public boolean hasFullHealth()
	{
		return this.getHealth() >= this.getMaxHealth();
	}

	public int getPercentMaxHealth(float percent)
	{
		return Math.max(1, (int) (this.getMaxHealth() * percent / 100.0F));
	}

	public void healByPercent(float percent)
	{
		this.healEntityBy(this.getPercentMaxHealth(percent));
	}

	public void setAttackFailed()
	{
		PixelmonWrapper pw = this.getPixelmonWrapper();
		if (pw != null && pw.attack != null && pw.attack.moveResult != null)
		{
			pw.attack.moveResult.result = AttackResult.failed;
		}

	}

	public int getPartyPosition()
	{
		int partyPosition = super.getPartyPosition();
		if (partyPosition == -1)
		{
			PixelmonWrapper pw = this.getPixelmonWrapper();
			if (pw != null)
			{
				partyPosition = pw.getPartyPosition();
			}
		}

		return partyPosition;
	}
}