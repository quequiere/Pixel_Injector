/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.battles.controller;

import com.pixelmonmod.pixelmon.PixelmonMethods;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.ParticipantType;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.TrainerParticipant;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.EVsStore;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Level;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.StatsType;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.Evolution;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.EvolutionLevel;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.evolution.EvolutionType;
import com.pixelmonmod.pixelmon.enums.heldItems.EnumHeldItems;
import com.pixelmonmod.pixelmon.items.ItemExpAll;
import com.pixelmonmod.pixelmon.items.ItemHeld;
import com.pixelmonmod.pixelmon.items.heldItems.EVAdjusting;
import com.pixelmonmod.pixelmon.items.heldItems.ItemLuckyEgg;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;

public final class Experience
{
	public static void awardExp(ArrayList<BattleParticipant> participants, BattleParticipant losingTeamOwner, EntityPixelmon faintedPokemon)
	{
		if (!PixelmonConfig.allowPVPExperience)
		{
			boolean allPlayers = true;
			Iterator teamOwner = participants.iterator();

			while (teamOwner.hasNext())
			{
				BattleParticipant player = (BattleParticipant) teamOwner.next();
				if (player.getType() != ParticipantType.Player)
				{
					allPlayers = false;
				}
			}

			if (allPlayers)
			{
				return;
			}
		}

		Iterator arg19;
		BattleParticipant arg20;
		if (!PixelmonConfig.allowTrainerExperience)
		{
			arg19 = participants.iterator();

			while (arg19.hasNext())
			{
				arg20 = (BattleParticipant) arg19.next();
				if (arg20.getType() == ParticipantType.Trainer)
				{
					return;
				}
			}
		}

		arg19 = participants.iterator();

		while (true)
		{
			do
			{
				do
				{
					if (!arg19.hasNext())
					{
						return;
					}

					arg20 = (BattleParticipant) arg19.next();
				}
				while (arg20.team == losingTeamOwner.team);
			}
			while (!(arg20 instanceof PlayerParticipant));

			PlayerParticipant arg21 = (PlayerParticipant) arg20;
			if (arg21.initialLevels != null)
			{
				return;
			}

			ArrayList enemyIdList = arg21.attackersList;
			PlayerStorage storage = arg21.getStorage();
			if (storage == null)
			{
				return;
			}

			ArrayList doneUsers = new ArrayList();
			PixelmonWrapper[] numPokemon = arg20.controlledPokemon;
			int hasExpAll = numPokemon.length;

			int[] id;
			for (int ido = 0; ido < hasExpAll; ++ido)
			{
				PixelmonWrapper pokemon = numPokemon[ido];
				boolean contains = false;
				Iterator nbt = enemyIdList.iterator();

				while (nbt.hasNext())
				{
					id = (int[]) nbt.next();
					if (PixelmonMethods.isIDSame(id, pokemon.pokemon))
					{
						contains = true;
					}
				}

				if (!contains)
				{
					enemyIdList.add(pokemon.pokemon.getPokemonId());
				}
			}

			int arg22;
			for (arg22 = 0; arg22 < enemyIdList.size(); ++arg22)
			{
				if (storage.isFainted((int[]) enemyIdList.get(arg22)))
				{
					enemyIdList.remove(arg22);
					--arg22;
				}
			}

			arg22 = calcUniqueParticipants(enemyIdList);
			Iterator arg23 = enemyIdList.iterator();

			while (arg23.hasNext())
			{
				int[] arg25 = (int[]) arg23.next();
				if (!contains(doneUsers, arg25))
				{
					calcExp(storage, faintedPokemon, arg25, 1.0D, arg22);
					doneUsers.add(arg25);
				}
			}

			boolean arg24 = false;
			ItemStack[][] arg26 = new ItemStack[][] { arg21.player.inventory.mainInventory, arg21.player.inventory.offHandInventory };
			int arg27 = arg26.length;

			int arg29;
			for (arg29 = 0; arg29 < arg27; ++arg29)
			{
				ItemStack[] arg30 = arg26[arg29];
				ItemStack[] arg32 = arg30;
				int getsExp = arg30.length;

				for (int itemStack = 0; itemStack < getsExp; ++itemStack)
				{
					ItemStack item = arg32[itemStack];
					if (item != null && item.getItem() instanceof ItemExpAll && item.hasTagCompound())
					{
						NBTTagCompound nbt1 = item.getTagCompound();
						if (nbt1.getBoolean("Activated"))
						{
							arg24 = true;
							break;
						}
					}
				}
			}

			NBTTagCompound[] arg28 = storage.getList();
			arg27 = arg28.length;

			for (arg29 = 0; arg29 < arg27; ++arg29)
			{
				NBTTagCompound arg31 = arg28[arg29];
				if (arg31 != null && !arg31.getBoolean("IsFainted") && arg31.getShort("Health") > 0 && !arg31.getBoolean("isEgg"))
				{
					id = PixelmonMethods.getID(arg31);
					if (!contains(doneUsers, id))
					{
						boolean arg33 = false;
						if (arg24)
						{
							arg33 = true;
						}
						else if (arg31.hasKey("HeldItemStack"))
						{
							ItemStack arg34 = ItemStack.loadItemStackFromNBT((NBTTagCompound) arg31.getTag("HeldItemStack"));
							if (arg34 != null)
							{
								Item arg35 = arg34.getItem();
								if (arg35 instanceof ItemHeld && ((ItemHeld) arg35).getHeldItemType() == EnumHeldItems.expShare)
								{
									arg33 = true;
								}
							}
						}

						if (arg33)
						{
							calcExp(storage, faintedPokemon, PixelmonMethods.getID(arg31), 0.5D, arg22);
						}
					}
				}
			}
		}
	}

	private static int calcUniqueParticipants(ArrayList<int[]> enemyIdList)
	{
		ArrayList doneUsers = new ArrayList();
		enemyIdList.stream().filter((ido) -> {
			return !contains(doneUsers, ido);
		}).forEach(doneUsers::add);
		return doneUsers.size();
	}

	private static boolean contains(ArrayList<int[]> doneUsers, int[] ido)
	{
		Iterator arg1 = doneUsers.iterator();

		int[] doneUser;
		do
		{
			if (!arg1.hasNext())
			{
				return false;
			}

			doneUser = (int[]) arg1.next();
		}
		while (!PixelmonMethods.isIDSame(doneUser, ido));

		return true;
	}

	private static void calcExp(PlayerStorage storage, EntityPixelmon faintedPokemon, int[] ido, double scaleFactor, int numPokemon)
	{
		NBTTagCompound expReceiver = null;
		expReceiver = storage.getNBT(ido);
		if (expReceiver != null)
		{
			double a = 1.0D;
			if (faintedPokemon.getParticipant() instanceof TrainerParticipant || faintedPokemon.getParticipant() instanceof PlayerParticipant)
			{
				a = 1.5D;
			}

			double t = expReceiver.getString("originalTrainer").equals(storage.getDisplayName()) ? 1.0D : 1.5D;
			double baseExp = (double) faintedPokemon.baseStats.baseExp;
			double eggMultiplier = 1.0D;
			NBTTagCompound nbt = storage.getNBT(ido);
			if (nbt.hasKey("HeldItemStack"))
			{
				ItemStack itemStack = ItemStack.loadItemStackFromNBT((NBTTagCompound) nbt.getTag("HeldItemStack"));
				if (itemStack != null)
				{
					Item item = itemStack.getItem();
					if (item instanceof ItemLuckyEgg)
					{
						eggMultiplier = 1.5D;
					}
				}
			}

			double faintedLevel = (double) faintedPokemon.getLvl().getLevel();
			double s = (double) numPokemon;
			if (s == 0.0D)
			{
				s = 1.0D;
			}

			scaleFactor *= (double) PixelmonConfig.expModifier;
			
			
			//quequiere
			for(PotionEffect pe:storage.getPlayer().getActivePotionEffects())
			{
				if(pe.getPotion().getName().equals("ExpPokemonMultiplier"))
				{
					scaleFactor*=pe.getAmplifier();
				}
			}
			//end quequiere ----
			
			
			double exp = a * t * baseExp * eggMultiplier * faintedLevel * scaleFactor / 7.0D;
			EntityPixelmon pokemon = null;
			if (storage.isInWorld(ido))
			{
				Optional level = storage.getAlreadyExists(ido, faintedPokemon.worldObj);
				if (level.isPresent())
				{
					pokemon = (EntityPixelmon) level.get();
				}
			}
			else
			{
				pokemon = storage.sendOut(ido, faintedPokemon.worldObj);
			}

			if (pokemon != null)
			{
				Level arg30 = pokemon.getLvl();
				Evolution[] evStore = pokemon.baseStats.evolutions;
				int heldItem = evStore.length;

				for (int item1 = 0; item1 < heldItem; ++item1)
				{
					Evolution e = evStore[item1];
					if (e.data != null && e.data.getType() == EvolutionType.Level)
					{
						EvolutionLevel evoLevel = (EvolutionLevel) e.data;
						if (arg30.getLevel() >= evoLevel.getEvolutionLevel())
						{
							exp *= 1.2D;
							break;
						}
					}
				}

				arg30.awardEXP((int) exp);
				EVsStore arg31 = faintedPokemon.baseStats.evGain.cloneEVs();
				ItemStack arg32 = pokemon.getHeldItemMainhand();
				if (ItemHeld.isItemOfType(arg32, EnumHeldItems.evAdjusting))
				{
					EVAdjusting arg33 = (EVAdjusting) arg32.getItem();
					if (arg33.type.statAffected == StatsType.None)
					{
						arg31.doubleValues();
					}
					else
					{
						arg31.addEVs(4, arg33.type.statAffected);
					}
				}

				pokemon.stats.EVs.gainEV(arg31);
				pokemon.updateStats();
				storage.update(pokemon, EnumUpdateType.Stats);
			}

		}
	}
}