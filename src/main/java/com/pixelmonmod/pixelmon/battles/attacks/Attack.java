/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.pixelmonmod.pixelmon.battles.attacks;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.api.events.PixelmonCalcCritEvent;
import com.pixelmonmod.pixelmon.battles.attacks.AttackBase;
import com.pixelmonmod.pixelmon.battles.attacks.DamageTypeEnum;
import com.pixelmonmod.pixelmon.battles.attacks.EffectBase;
import com.pixelmonmod.pixelmon.battles.attacks.TargetingInfo;
import com.pixelmonmod.pixelmon.battles.attacks.animations.IAttackAnimation;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.StatsEffect;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.attackModifiers.CriticalHit;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.attackModifiers.MultipleHit;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.Assurance;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.BeatUp;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.Fling;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.TripleKick;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.MoveChoice;
import com.pixelmonmod.pixelmon.battles.controller.log.AttackResult;
import com.pixelmonmod.pixelmon.battles.controller.log.MoveResults;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.battles.status.StatusBase;
import com.pixelmonmod.pixelmon.battles.status.StatusType;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.comm.PixelmonData;
import com.pixelmonmod.pixelmon.comm.packetHandlers.clientStorage.Add;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.database.AttackCategory;
import com.pixelmonmod.pixelmon.database.DatabaseMoves;
import com.pixelmonmod.pixelmon.entities.npcs.NPCTrainer;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.AbilityBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.AngerPoint;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.KeenEye;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Overcoat;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.ParentalBond;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Sniper;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.SuperLuck;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Unaware;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.StatsType;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.items.ItemHeld;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.translation.I18n;

public class Attack
{
	public static final float EFFECTIVE_NORMAL = 1.0F;
	public static final float EFFECTIVE_SUPER = 2.0F;
	public static final float EFFECTIVE_MAX = 4.0F;
	public static final float EFFECTIVE_NOT = 0.5F;
	public static final float EFFECTIVE_BARELY = 0.25F;
	public static final float EFFECTIVE_NONE = 0.0F;
	public static final int ATTACK_PHYSICAL = 0;
	public static final int ATTACK_SPECIAL = 1;
	public static final int ATTACK_STATUS = 2;
	public static final int NEVER_MISS = -1;
	public static final int IGNORE_SEMIINVULNERABLE = -2;
	private static final int NUM_ATTACKS = 650;
	private static AttackBase[] fullAttackList;
	private static Map<String, AttackBase> fullAttackMap;
	public AttackBase baseAttack;
	public int pp;
	public int ppBase;
	public int movePower;
	public int moveAccuracy;
	public boolean cantMiss;
	private boolean disabled;
	public MoveResults moveResult;
	public float damageResult;
	public int savedPower;
	public int savedAccuracy;
	public EnumType savedType;
	static AttackCategory[] categories;

	public Attack(int attackIndex, String moveName, ResultSet rs) throws SQLException
	{
		initializeAllAttacks();
		AttackBase base = fullAttackList[attackIndex];
		if (base == null)
		{
			Pixelmon.LOGGER.error("No attack found with name: " + moveName);
		}

		this.initializeAttack(base);
	}

	public Attack(AttackBase base)
	{
		this.initializeAttack(base);
	}

	public Attack(int attackIndex)
	{
		initializeAllAttacks();

		try
		{
			this.initializeAttack(fullAttackList[attackIndex]);
		}
		catch (NullPointerException | IndexOutOfBoundsException arg2)
		{
			Pixelmon.LOGGER.error("No attack found for index " + attackIndex);
		}

	}

	public Attack(String moveName)
	{
		initializeAllAttacks();
		if (hasAttack(moveName))
		{
			this.initializeAttack((AttackBase) getAttackBase(moveName).get());
		}
		else
		{
			Pixelmon.LOGGER.error("No attack found with name: " + moveName);
		}

	}

	public static Optional<AttackBase> getAttackBase(String moveName)
	{
		return hasAttack(moveName) ? Optional.of(fullAttackMap.get(moveName.toLowerCase())) : Optional.empty();
	}

	public void initializeAttack(AttackBase base)
	{
		this.baseAttack = base;
		this.movePower = this.baseAttack.basePower;
		this.pp = this.baseAttack.ppBase;
		this.ppBase = this.baseAttack.ppBase;
		fullAttackMap.put(base.getLocalizedName(), base);
	}

	public static boolean hasAttack(int attackIndex)
	{
		initializeAllAttacks();
		return fullAttackList[attackIndex] != null;
	}

	public static boolean hasAttack(String moveName)
	{
		initializeAllAttacks();
		return fullAttackMap.containsKey(moveName.toLowerCase());
	}

	public static AttackBase[] getAttacks(String[] nameList)
	{
		initializeAllAttacks();
		AttackBase[] attacks = new AttackBase[nameList.length];

		for (int i = 0; i < nameList.length; ++i)
		{
			attacks[i] = (AttackBase) fullAttackMap.get(nameList[i].toLowerCase());
			if (attacks[i] == null)
			{
				Pixelmon.LOGGER.error("No attack found with name: " + nameList[i]);
			}
		}

		return attacks;
	}

	private static void initializeAllAttacks()
	{
		if (fullAttackList == null)
		{
			fullAttackList = new AttackBase[650];
			fullAttackMap = new HashMap(1300);
			DatabaseMoves.initializeAllAttacks();
		}

	}

	public static void registerAttack(int attackIndex, String moveName, ResultSet rs) throws SQLException
	{
		if (fullAttackList[attackIndex] == null)
		{
			AttackBase a = new AttackBase(attackIndex, moveName, rs);
			fullAttackList[attackIndex] = a;
			fullAttackMap.put(moveName.toLowerCase(), a);
			fullAttackMap.put(a.getLocalizedName().toLowerCase(), a);
		}

	}

	public boolean use(PixelmonWrapper user, PixelmonWrapper target, MoveResults moveResults)
	{
		this.moveResult = moveResults;
		this.damageResult = -1.0F;
		if (user.bc != null && target.bc != null)
		{
			if (user.pokemon.battleController == null)
			{
				user.pokemon.battleController = user.bc;
			}

			if (target.pokemon.battleController == null)
			{
				target.pokemon.battleController = target.bc;
			}

			EntityPixelmon userPokemon = user.pokemon;
			EntityPixelmon targetPokemon = target.pokemon;
			AbilityBase userAbility = userPokemon.getBattleAbility();
			AbilityBase targetAbility = targetPokemon.getBattleAbility();
			if (!this.canHit(userPokemon, targetPokemon) && !this.isAttack(new String[] { "Doom Desire", "Future Sight", "Imprison", "Spikes", "Stealth Rock", "Toxic Spikes" }))
			{
				moveResults.result = AttackResult.notarget;
				return true;
			}
			else
			{
				Iterator arg30;
				if (user == target)
				{
					arg30 = user.bc.getActiveUnfaintedPokemon().iterator();

					while (arg30.hasNext())
					{
						PixelmonWrapper arg35 = (PixelmonWrapper) arg30.next();
						Iterator arg36 = arg35.pokemon.getStatuses().iterator();

						while (arg36.hasNext())
						{
							StatusBase arg38 = (StatusBase) arg36.next();
							if (arg38.stopsSelfStatusMove(arg35, user, this))
							{
								moveResults.result = AttackResult.failed;
								return true;
							}
						}
					}

					this.applySelfStatusMove(user, moveResults);
					return true;
				}
				else
				{
					Iterator saveAccuracy;
					PixelmonWrapper beforeSize;
					Iterator i;
					StatusBase status;
					if (user.targets.size() == 1)
					{
						ArrayList modifiedMoveStats = user.bc.getOpponentPokemon(user.getParticipant());
						if (modifiedMoveStats.size() > 1)
						{
							saveAccuracy = modifiedMoveStats.iterator();

							label473:
							while (true)
							{
								do
								{
									if (!saveAccuracy.hasNext())
									{
										break label473;
									}

									beforeSize = (PixelmonWrapper) saveAccuracy.next();
								}
								while (beforeSize == target);

								i = beforeSize.pokemon.getStatuses().iterator();

								while (i.hasNext())
								{
									status = (StatusBase) i.next();
									if (status.redirectAttack(user, beforeSize, this))
									{
										target = beforeSize;
										targetPokemon = beforeSize.pokemon;
										break;
									}
								}

								if (beforeSize.pokemon.getBattleAbility().redirectAttack(user, beforeSize, this))
								{
									target = beforeSize;
									targetPokemon = beforeSize.pokemon;
									break;
								}
							}
						}
					}

					arg30 = this.baseAttack.effects.iterator();

					while (arg30.hasNext())
					{
						EffectBase arg32 = (EffectBase) arg30.next();

						try
						{
							if (arg32.ApplyEffectStart(user, target) != AttackResult.proceed)
							{
								return true;
							}
						}
						catch (Exception arg27)
						{
							arg27.printStackTrace();
						}
					}

					int[] arg9999 = new int[] { this.baseAttack.basePower, this.baseAttack.accuracy };
					int[] arg31 = userAbility.modifyPowerAndAccuracyUser(this.baseAttack.basePower, this.baseAttack.accuracy, userPokemon, targetPokemon, this);

					for (saveAccuracy = user.bc.getTeamPokemon(user).iterator(); saveAccuracy.hasNext(); arg31 = beforeSize.pokemon.getBattleAbility().modifyPowerAndAccuracyTeammate(arg31[0], arg31[1], userPokemon, targetPokemon, this))
					{
						beforeSize = (PixelmonWrapper) saveAccuracy.next();
					}

					arg31 = targetAbility.modifyPowerAndAccuracyTarget(arg31[0], arg31[1], userPokemon, targetPokemon, this);
					int arg33 = 0;
					if (arg31[1] < 0)
					{
						arg33 = arg31[1];
					}

					if (ItemHeld.canUseItem(userPokemon))
					{
						arg31 = userPokemon.getItemHeld().modifyPowerAndAccuracyUser(arg31, userPokemon, targetPokemon, this);
					}

					if (ItemHeld.canUseItem(targetPokemon))
					{
						arg31 = targetPokemon.getItemHeld().modifyPowerAndAccuracyTarget(arg31, userPokemon, targetPokemon, this);
					}

					int arg34 = userPokemon.getStatuses().size();

					for (int arg37 = 0; arg37 < arg34; ++arg37)
					{
						status = userPokemon.getStatus(arg37);
						arg31 = status.modifyPowerAndAccuracyUser(arg31[0], arg31[1], userPokemon, targetPokemon, this);
						if (userPokemon.getStatuses().size() != arg34)
						{
							break;
						}
					}

					for (i = targetPokemon.getStatuses().iterator(); i.hasNext(); arg31 = status.modifyPowerAndAccuracyTarget(arg31[0], arg31[1], userPokemon, targetPokemon, this))
					{
						status = (StatusBase) i.next();
					}

					for (i = user.bc.globalStatusController.getGlobalStatuses().iterator(); i.hasNext(); arg31 = status.modifyPowerAndAccuracyTarget(arg31[0], arg31[1], userPokemon, targetPokemon, this))
					{
						status = (StatusBase) i.next();
					}

					if (arg33 < 0 && arg31[1] >= 0)
					{
						arg31[1] = arg33;
					}

					this.movePower = arg31[0];
					this.moveAccuracy = Math.min(arg31[1], 100);
					this.cantMiss = false;
					userPokemon.getLookHelper().setLookPositionWithEntity(targetPokemon, 0.0F, 0.0F);
					double accuracy = (double) this.moveAccuracy;
					if (this.moveAccuracy >= 0)
					{
						int allStatuses = targetPokemon.battleStats.getEvasionStage();
						if (user.bc.globalStatusController.hasStatus(StatusType.Gravity))
						{
							allStatuses = Math.max(-6, allStatuses - 2);
						}

						if (user.pokemon.getBattleAbility() instanceof KeenEye)
						{
							allStatuses = Math.min(0, allStatuses);
						}

						double combinedAccuracy = (double) (userPokemon.battleStats.getAccuracyStage() - allStatuses);
						if (combinedAccuracy > 6.0D)
						{
							combinedAccuracy = 6.0D;
						}
						else if (combinedAccuracy < -6.0D)
						{
							combinedAccuracy = -6.0D;
						}

						accuracy = (double) this.moveAccuracy * ((double) userPokemon.battleStats.GetAccOrEva(combinedAccuracy) / 100.0D);
					}

					ArrayList arg39 = (ArrayList) targetPokemon.getStatuses().stream().collect(Collectors.toList());
					arg39.addAll((Collection) target.bc.globalStatusController.getGlobalStatuses().stream().collect(Collectors.toList()));
					boolean shouldNotLosePP = false;

					EffectBase critModifier;
					for (int hasNoEffect = 0; hasNoEffect < this.baseAttack.effects.size(); ++hasNoEffect)
					{
						critModifier = (EffectBase) this.baseAttack.effects.get(hasNoEffect);
						if (critModifier instanceof MultiTurnSpecialAttackBase)
						{
							shouldNotLosePP = ((MultiTurnSpecialAttackBase) critModifier).shouldNotLosePP(userPokemon);
						}
					}

					Iterator arg40 = userPokemon.getStatuses().iterator();

					StatusBase arg41;
					while (arg40.hasNext())
					{
						arg41 = (StatusBase) arg40.next();

						try
						{
							if (arg41.stopsIncomingAttackUser(target, user))
							{
								return !shouldNotLosePP;
							}
						}
						catch (Exception arg26)
						{
							user.bc.battleLog.onCrash(arg26, "Error calculating stopsIncomingAttack for " + arg41.type.toString() + " for attack " + this.baseAttack.getLocalizedName());
						}
					}

					arg40 = arg39.iterator();

					while (arg40.hasNext())
					{
						arg41 = (StatusBase) arg40.next();

						try
						{
							if (arg41.stopsIncomingAttack(target, user))
							{
								this.onMiss(user, target, moveResults, arg41);
								return !shouldNotLosePP;
							}
						}
						catch (Exception arg25)
						{
							user.bc.battleLog.onCrash(arg25, "Error calculating stopsIncomingAttack for " + arg41.type.toString() + " for attack " + this.baseAttack.getLocalizedName());
						}
					}

					if (!targetPokemon.getBattleAbility(userPokemon).allowsIncomingAttack(targetPokemon, userPokemon, this) || ItemHeld.canUseItem(targetPokemon) && !targetPokemon.getItemHeld().allowsIncomingAttack(targetPokemon, userPokemon, this))
					{
						try
						{
							this.onMiss(user, target, moveResults, targetAbility);
							return !shouldNotLosePP;
						}
						catch (Exception arg29)
						{
							user.bc.battleLog.onCrash(arg29, "Error calculating allowsIncomingAttack for attack " + this.baseAttack.getLocalizedName());
						}
					}

					boolean arg42 = this.getTypeEffectiveness(user, target) == 0.0D && (this.baseAttack.attackCategory != 2 || this.isAttack(new String[] { "Poison Gas", "Poison Powder", "Thunder Wave", "Toxic" }));
					if (!arg42 && target.pokemon.type.contains(EnumType.Grass) && Overcoat.isPowderMove(this))
					{
						user.bc.sendToAll("pixelmon.battletext.noeffect", new Object[] { target.pokemon.getNickname() });
						arg42 = true;
					}

					for (int arg43 = 0; arg43 < this.baseAttack.effects.size(); ++arg43)
					{
						if (this.baseAttack.effects.get(arg43) instanceof MultiTurnSpecialAttackBase && ((MultiTurnSpecialAttackBase) this.baseAttack.effects.get(arg43)).ignoresType(userPokemon))
						{
							arg42 = false;
							break;
						}
					}

					if (arg42)
					{
						this.onMiss(user, target, moveResults, EnumType.Mystery);
						return !shouldNotLosePP;
					}
					else
					{
						if (!shouldNotLosePP)
						{
							targetAbility.preProcessAttack(targetPokemon, userPokemon, this);
						}

						this.cantMiss = this.cantMiss(user) || this.moveAccuracy < 0;
						if (user.bc.simulateMode)
						{
							this.moveResult.accuracy = this.moveAccuracy;
							accuracy = 100.0D;
						}

						critModifier = null;
						if (!this.cantMiss && !RandomHelper.getRandomChance((int) accuracy))
						{
							this.onMiss(user, target, moveResults, (Object) null);
						}
						else
						{
							AttackResult i1 = AttackResult.proceed;
							AttackResult sizeBefore = AttackResult.proceed;
							Iterator arg22 = this.baseAttack.effects.iterator();

							EffectBase e;
							while (arg22.hasNext())
							{
								e = (EffectBase) arg22.next();

								try
								{
									if (e instanceof AttackModifierBase)
									{
										if (e instanceof CriticalHit)
										{
											critModifier = e;
										}
										else
										{
											sizeBefore = ((AttackModifierBase) e).ApplyEffectDuring(user, target);
											if (sizeBefore != AttackResult.proceed)
											{
												i1 = sizeBefore;
											}
										}
									}
									else if (e instanceof SpecialAttackBase)
									{
										sizeBefore = ((SpecialAttackBase) e).ApplyEffectDuring(user, target);
										if (sizeBefore != AttackResult.proceed)
										{
											i1 = sizeBefore;
										}
									}
									else if (e instanceof MultiTurnSpecialAttackBase)
									{
										sizeBefore = ((MultiTurnSpecialAttackBase) e).ApplyEffectDuring(user, target);
										if (sizeBefore != AttackResult.proceed)
										{
											break;
										}
									}

									if (i1 != AttackResult.succeeded && i1 != AttackResult.succeeded && i1 != AttackResult.failed && i1 != AttackResult.charging && i1 != AttackResult.notarget)
									{
										if (i1 == AttackResult.hit)
										{
											if (target.pokemon.getHealth() > 0.0F)
											{
												moveResults.result = AttackResult.hit;
											}
											else
											{
												moveResults.result = AttackResult.killed;
											}
										}
									}
									else
									{
										moveResults.result = i1;
									}
								}
								catch (Exception arg28)
								{
									user.bc.battleLog.onCrash(arg28, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.baseAttack.getLocalizedName());
								}
							}

							if (sizeBefore == AttackResult.proceed)
							{
								if (userAbility instanceof ParentalBond)
								{
									user.inMultipleHit = true;
									user.inParentalBond = true;
									arg22 = this.baseAttack.effects.iterator();

									label342:
									while (true)
									{
										while (true)
										{
											if (!arg22.hasNext())
											{
												break label342;
											}

											e = (EffectBase) arg22.next();
											if (!(e instanceof BeatUp) && !(e instanceof Fling) && !(e instanceof MultiTurnCharge) && !(e instanceof MultipleHit) && !(e instanceof TripleKick))
											{
												if (e instanceof Assurance)
												{
													this.baseAttack.basePower *= 2;
												}
											}
											else
											{
												user.inMultipleHit = false;
												user.inParentalBond = false;
											}
										}
									}
								}

								this.executeAttackEffects(user, target, moveResults, critModifier, 1.0F);
								if (user.inParentalBond && this.baseAttack.attackCategory != 2 && !target.getIsFainted() && !user.getIsFainted() && user.targets.size() == 1)
								{
									arg22 = this.baseAttack.effects.iterator();

									while (arg22.hasNext())
									{
										e = (EffectBase) arg22.next();
										if (e instanceof Assurance)
										{
											this.baseAttack.basePower *= 2;
										}
									}

									user.inMultipleHit = false;
									user.inParentalBond = false;
									this.executeAttackEffects(user, target, moveResults, critModifier, 0.5F);
									user.bc.sendToAll("multiplehit.times", new Object[] { user.pokemon.getNickname(), Integer.valueOf(2) });
								}

								user.inParentalBond = false;
								this.doMove(user, target);
							}
						}

						for (int arg44 = 0; arg44 < target.pokemon.getStatusSize(); ++arg44)
						{
							int arg45 = target.pokemon.getStatusSize();
							target.pokemon.getStatus(arg44).onAttackEnd(target);
							if (arg45 > target.pokemon.getStatusSize())
							{
								--arg44;
							}
						}

						if (!user.bc.simulateMode)
						{
							if (userPokemon.getOwner() != null)
							{
								userPokemon.update(new EnumUpdateType[] { EnumUpdateType.HP, EnumUpdateType.Moveset });
							}

							if (targetPokemon.getOwner() != null)
							{
								targetPokemon.update(new EnumUpdateType[] { EnumUpdateType.HP, EnumUpdateType.Moveset });
							}

							if (userPokemon.getTrainer() != null)
							{
								userPokemon.getTrainer().getPokemonStorage().updateAndSendToClient(userPokemon, new EnumUpdateType[] { EnumUpdateType.Moveset, EnumUpdateType.HP });
							}

							if (targetPokemon.getTrainer() != null)
							{
								targetPokemon.getTrainer().getPokemonStorage().updateAndSendToClient(targetPokemon, new EnumUpdateType[] { EnumUpdateType.Moveset, EnumUpdateType.HP });
							}
						}

						if (targetPokemon.getHealth() <= 0.0F)
						{
							shouldNotLosePP = false;
						}

						return !shouldNotLosePP;
					}
				}
			}
		}
		else
		{
			return false;
		}
	}

	private void executeAttackEffects(PixelmonWrapper user, PixelmonWrapper target, MoveResults moveResults, EffectBase critModifier, float damageMultiplier)
	{
		double crit = calcCriticalHit(critModifier, user, target);
		int power = (int) ((float) this.doDamageCalc(user, target, crit) * damageMultiplier);
		this.damageResult = (float) power;
		EntityPixelmon targetPokemon = target.pokemon;
		if (this.baseAttack.attackCategory == 2)
		{
			boolean power1 = false;
		}
		else if (targetPokemon.getHealth() > 0.0F)
		{
			if (crit > 1.0D)
			{
				if (user.targets.size() > 1)
				{
					user.bc.sendToAll("pixelmon.battletext.criticalhittarget", new Object[] { targetPokemon.getNickname() });
				}
				else
				{
					user.bc.sendToAll("pixelmon.battletext.criticalhit", new Object[0]);
				}
			}

			this.damageResult = targetPokemon.doBattleDamage(user, (float) power, DamageTypeEnum.ATTACK);
			if (targetPokemon.getHealth() > 0.0F)
			{
				moveResults.result = AttackResult.hit;
			}
			else
			{
				moveResults.result = AttackResult.killed;
			}
		}
		else
		{
			this.damageResult = -1.0F;
			moveResults.result = AttackResult.notarget;
		}

		AttackResult tempResult = this.applyAttackEffect(user, target);
		if (tempResult != null)
		{
			moveResults.result = tempResult;
		}

		applyContactLate(user, target);
		if (ItemHeld.canUseItem(targetPokemon) && this.canRemoveBerry())
		{
			targetPokemon.getItemHeld().tookDamage(user.pokemon, targetPokemon, this.damageResult, DamageTypeEnum.ATTACK);
		}

	}

	public void doMove(PixelmonWrapper user, PixelmonWrapper target)
	{
		if (!user.bc.simulateMode)
		{
			if (user.pokemon.posX == 0.0D && user.pokemon.posY == 0.0D && user.pokemon.posZ == 0.0D)
			{
				EntityLivingBase owner = user.getParticipant().getEntity();
				if (owner != null && !user.bc.simulateMode)
				{
					user.pokemon.setLocationAndAngles(owner.posX, owner.posY, owner.posZ, owner.rotationYaw, 0.0F);
				}
			}

			Iterator owner1 = this.baseAttack.animations.iterator();

			while (owner1.hasNext())
			{
				IAttackAnimation anim = (IAttackAnimation) owner1.next();
				anim.doMove(user.pokemon, target.pokemon);
			}

		}
	}

	public int doDamageCalc(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper, double crit)
	{
		if (this.movePower <= 0)
		{
			return 0;
		}
		else
		{
			EntityPixelmon user = userWrapper.pokemon;
			EntityPixelmon target = targetWrapper.pokemon;
			AbilityBase userAbility = user.getBattleAbility();
			double stab = 1.0D;
			if (this.hasSTAB(user))
			{
				stab = 1.5D;
			}

			stab = userAbility.modifyStab(stab);
			double type = this.getTypeEffectiveness(userWrapper, targetWrapper);
			double modifier = stab * type * crit;
			double attack = 0.0D;
			double defense = 0.0D;
			double Level = (double) user.getLvl().getLevel();
			StatsType attackStat;
			StatsType defenseStat;
			if (this.baseAttack.attackCategory == 1)
			{
				attackStat = StatsType.SpecialAttack;
				if (this.isAttack(new String[] { "Psyshock", "Psystrike", "Secret Sword" }))
				{
					defenseStat = StatsType.Defence;
				}
				else
				{
					defenseStat = StatsType.SpecialDefence;
				}
			}
			else
			{
				attackStat = StatsType.Attack;
				defenseStat = StatsType.Defence;
			}

			attack = (double) user.battleStats.getStatWithMod(attackStat);
			defense = (double) target.battleStats.getStatWithMod(defenseStat);
			if (crit > 1.0D)
			{
				attack = Math.max((double) user.battleStats.getStatFromEnum(attackStat), attack);
				defense = Math.min((double) target.battleStats.getStatFromEnum(defenseStat), defense);
			}

			if (this.isAttack(new String[] { "Chip Away", "Sacred Sword" }) || userAbility instanceof Unaware)
			{
				defense = (double) target.battleStats.getStatFromEnum(defenseStat);
			}

			if (this.isAttack(new String[] { "Beat Up" }) || target.getBattleAbility(user) instanceof Unaware)
			{
				attack = (double) user.battleStats.getStatFromEnum(attackStat);
			}

			double DmgRand = (double) RandomHelper.getRandomNumberBetween(85, 100) / 100.0D;
			if (userWrapper.bc.simulateMode)
			{
				DmgRand = 1.0D;
			}

			double DamageBase = (2.0D * Level / 5.0D + 2.0D) * attack * (double) this.movePower * 0.02D / defense + 2.0D;
			double Damage = DamageBase * modifier * DmgRand;
			if (ItemHeld.canUseItem(user))
			{
				Damage = user.getItemHeld().preProcessAttackUser(user, target, this, Damage);
			}

			if (ItemHeld.canUseItem(target))
			{
				Damage = target.getItemHeld().preProcessAttackTarget(user, target, this, Damage);
			}

			if (userWrapper.targets.size() > 1)
			{
				Damage *= 0.75D;
			}

			if (Damage < 1.0D && Damage > 0.0D && this.movePower > 0)
			{
				Damage = 1.0D;
			}

			return (int) Damage;
		}
	}

	public void applySelfStatusMove(PixelmonWrapper user, MoveResults moveResults)
	{
		EntityPixelmon userPokemon = user.pokemon;
		userPokemon.getLookHelper().setLookPositionWithEntity(userPokemon, 0.0F, 0.0F);

		for (int trainer = 0; trainer < this.baseAttack.effects.size(); ++trainer)
		{
			EffectBase e = (EffectBase) this.baseAttack.effects.get(trainer);

			try
			{
				if (e instanceof StatsEffect)
				{
					AttackResult exc = ((StatsEffect) e).ApplyStatEffect(user, user, this.baseAttack);
					if (moveResults.result != AttackResult.succeeded)
					{
						moveResults.result = exc;
					}
				}
				else if (e instanceof SpecialAttackBase)
				{
					if (e.ApplyEffectStart(user, user) != AttackResult.proceed)
					{
						return;
					}

					this.moveResult.result = ((SpecialAttackBase) e).ApplyEffectDuring(user, user);
				}
				else if (e instanceof MultiTurnSpecialAttackBase)
				{
					this.moveResult.result = ((MultiTurnSpecialAttackBase) e).ApplyEffectDuring(user, user);
				}
				else if (e instanceof StatusBase)
				{
					e.ApplyEffect(user, user);
				}
				else if (e instanceof CriticalHit && !userPokemon.battleStats.IncreaseCritStage(2))
				{
					user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
				}
			}
			catch (Exception arg6)
			{
				user.bc.battleLog.onCrash(arg6, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.baseAttack.getLocalizedName());
			}
		}

		if (ItemHeld.canUseItem(user.pokemon))
		{
			user.pokemon.getItemHeld().onStatModified(userPokemon);
		}

		if (!user.bc.simulateMode)
		{
			if (userPokemon.getOwner() != null)
			{
				userPokemon.update(new EnumUpdateType[] { EnumUpdateType.HP, EnumUpdateType.Moveset });
			}

			NPCTrainer arg7 = userPokemon.getTrainer();
			if (arg7 != null)
			{
				arg7.getPokemonStorage().updateAndSendToClient(userPokemon, new EnumUpdateType[] { EnumUpdateType.Moveset, EnumUpdateType.HP });
			}
		}

		if (moveResults.result == AttackResult.proceed)
		{
			moveResults.result = AttackResult.succeeded;
		}

	}

	public AttackResult applyAttackEffect(PixelmonWrapper user, PixelmonWrapper target)
	{
		AttackResult returnResult = null;

		for (int j = 0; j < this.baseAttack.effects.size(); ++j)
		{
			EffectBase e = (EffectBase) this.baseAttack.effects.get(j);

			try
			{
				if (e instanceof StatsEffect)
				{
					StatsEffect exc = (StatsEffect) e;
					boolean i = target.pokemon.getBattleAbility(user.pokemon).allowsStatChange(target.pokemon, user.pokemon, exc);
					if (i)
					{
						Iterator et = target.bc.getTeamPokemon(target).iterator();

						while (et.hasNext())
						{
							PixelmonWrapper ally = (PixelmonWrapper) et.next();
							if (!ally.pokemon.getBattleAbility().allowsStatChangeTeammate(ally.pokemon, target.pokemon, user.pokemon, exc))
							{
								i = false;
								break;
							}
						}
					}

					if (i)
					{
						AttackResult arg12 = exc.ApplyStatEffect(user, target, this.baseAttack);
						if (returnResult != AttackResult.succeeded)
						{
							returnResult = arg12;
						}
					}
				}
				else if (e instanceof StatusBase)
				{
					boolean arg10 = true;

					for (int arg11 = 0; arg11 < target.pokemon.getStatusSize(); ++arg11)
					{
						StatusBase arg13 = target.pokemon.getStatus(arg11);
						if (user == target && arg13.stopsStatusChange(arg13.type, target.pokemon, user.pokemon))
						{
							arg10 = false;
							break;
						}
					}

					if (arg10)
					{
						e.ApplyEffect(user, target);
					}
				}
				else
				{
					e.ApplyEffect(user, target);
				}
			}
			catch (Exception arg9)
			{
				user.bc.battleLog.onCrash(arg9, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.baseAttack.getLocalizedName());
			}
		}

		if (ItemHeld.canUseItem(user.pokemon))
		{
			user.pokemon.getItemHeld().onStatModified(user.pokemon);
		}

		return returnResult;
	}

	public static void applyContact(EntityPixelmon userPokemon, EntityPixelmon targetPokemon)
	{
		if (!targetPokemon.hasStatus(new StatusType[] { StatusType.Substitute }))
		{
			if (ItemHeld.canUseItem(targetPokemon))
			{
				targetPokemon.getItemHeld().applyEffectOnContact(userPokemon, targetPokemon);
			}

			userPokemon.getBattleAbility().applyEffectOnContactUser(userPokemon, targetPokemon);
			targetPokemon.getBattleAbility().applyEffectOnContactTarget(userPokemon, targetPokemon);
		}

	}

	public static void applyContactLate(PixelmonWrapper user, PixelmonWrapper target)
	{
		if (user.attack != null && user.attack.baseAttack.getMakesContact() && !target.pokemon.hasStatus(new StatusType[] { StatusType.Substitute }))
		{
			target.pokemon.getBattleAbility().applyEffectOnContactTargetLate(user.pokemon, target.pokemon);
		}

	}

	public static void postProcessAttackAllHits(PixelmonWrapper user, PixelmonWrapper target, Attack attack, float power, DamageTypeEnum damageType)
	{
		if (!user.inParentalBond)
		{
			Iterator arg4 = attack.baseAttack.effects.iterator();

			while (arg4.hasNext())
			{
				EffectBase effect = (EffectBase) arg4.next();
				effect.dealtDamage(user, target, attack, damageType);
			}

			target.pokemon.getBattleAbility().tookDamageTargetAfterMove(user.pokemon, target.pokemon, attack);
			if (ItemHeld.canUseItem(target.pokemon))
			{
				target.pokemon.getItemHeld().postProcessAttackTarget(user, target, attack, power);
			}

			if (ItemHeld.canUseItem(user.pokemon))
			{
				user.pokemon.getItemHeld().dealtDamage(user.pokemon, target.pokemon, attack, damageType);
			}
		}

	}

	public void onMiss(PixelmonWrapper user, PixelmonWrapper target, MoveResults results, Object cause)
	{
		try
		{
			Iterator exc = this.baseAttack.effects.iterator();

			EffectBase effect;
			while (exc.hasNext())
			{
				effect = (EffectBase) exc.next();
				if (effect instanceof MultiTurnSpecialAttackBase && ((MultiTurnSpecialAttackBase) effect).isCharging(user.pokemon, target.pokemon))
				{
					results.result = ((MultiTurnSpecialAttackBase) effect).ApplyEffectDuring(user, target);
					if (results.result == AttackResult.charging)
					{
						return;
					}
				}
			}

			if (cause instanceof StatusBase)
			{
				((StatusBase) cause).stopsIncomingAttackMessage(target, user);
			}
			else if (cause instanceof AbilityBase)
			{
				((AbilityBase) cause).allowsIncomingAttackMessage(target.pokemon, user.pokemon, this);
			}
			else if (cause instanceof EnumType)
			{
				user.bc.sendToAll("pixelmon.battletext.noeffect", new Object[] { target.pokemon.getNickname() });
			}
			else
			{
				user.bc.sendToAll("pixelmon.battletext.missedattack", new Object[] { target.pokemon.getNickname() });
				results.result = AttackResult.missed;
			}

			exc = this.baseAttack.effects.iterator();

			while (exc.hasNext())
			{
				effect = (EffectBase) exc.next();
				effect.ApplyMissEffect(user, target);
			}

			if (ItemHeld.canUseItem(user.pokemon))
			{
				user.pokemon.getItemHeld().onMiss(user.pokemon, target.pokemon);
			}
		}
		catch (Exception arg6)
		{
			user.bc.battleLog.onCrash(arg6, "Error in applyMissEffect for attack " + this.baseAttack.getLocalizedName());
		}

		if (results.result != AttackResult.missed)
		{
			results.result = AttackResult.failed;
		}

	}

	public boolean hasSTAB(EntityPixelmon user)
	{
		return user.type.contains(this.baseAttack.attackType);
	}

	public void setDisabled(boolean value, EntityPixelmon pixelmon)
	{
		this.setDisabled(value, pixelmon, false);
	}

	public void setDisabled(boolean value, EntityPixelmon pixelmon, boolean switching)
	{
		this.disabled = value;
		if (pixelmon.battleController != null && pixelmon.getOwner() != null && pixelmon.getParticipant() instanceof PlayerParticipant)
		{
			Add p = new Add(new PixelmonData(pixelmon, pixelmon.battleController.battleEnded || switching));
			Pixelmon.network.sendTo(p, (EntityPlayerMP) pixelmon.getOwner());
		}

	}

	public boolean getDisabled()
	{
		return this.disabled;
	}

	public static double calcCriticalHit(EffectBase e, PixelmonWrapper user, PixelmonWrapper target)
	{
		if (!target.pokemon.getBattleAbility(user.pokemon).preventsCriticalHits(user) && !target.pokemon.hasStatus(new StatusType[] { StatusType.LuckyChant }))
		{
			int critStage = 1;
			if (ItemHeld.canUseItem(user.pokemon))
			{
				critStage += user.pokemon.getItemHeld().adjustCritStage(user.pokemon);
			}

			AbilityBase userAbility = user.pokemon.getBattleAbility();
			if (userAbility instanceof SuperLuck)
			{
				++critStage;
			}

			if (user.attack.isAttack(new String[] { "Focus Energy" }) && !user.pokemon.battleStats.IncreaseCritStage(2))
			{
				user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
				user.attack.moveResult.result = AttackResult.failed;
			}

			float percent = 0.0625F;
			if (e != null && e instanceof CriticalHit)
			{
				critStage += e.value;
			}

			critStage += user.pokemon.battleStats.getCritStage();
			if (critStage == 2)
			{
				percent = 0.125F;
			}
			else if (critStage == 3)
			{
				percent = 0.5F;
			}
			else if (critStage >= 4)
			{
				percent = 1.0F;
			}

			if (user.bc.simulateMode && percent < 1.0F)
			{
				percent = 0.0F;
			}

			if (RandomHelper.getRandomChance(percent))
			{
				AbilityBase targetAbility = target.pokemon.getBattleAbility();
				if (targetAbility instanceof AngerPoint && !user.bc.simulateMode)
				{
					((AngerPoint) targetAbility).wasCrit = true;
				}

				double crit = 1.5D;
				if (userAbility instanceof Sniper)
				{
					crit *= 1.5D;
				}

				//quequiere
				PixelmonCalcCritEvent critevent = new PixelmonCalcCritEvent(e, user, target,crit);
				Pixelmon.EVENT_BUS.post(critevent);
				
				return critevent.getCrit();
			}
			else
			{
				//quequiere
				PixelmonCalcCritEvent critevent = new PixelmonCalcCritEvent(e, user, target,1.0D);
				Pixelmon.EVENT_BUS.post(critevent);
				
				return critevent.getCrit();
			}
		}
		else
		{
			//quequiere
			PixelmonCalcCritEvent critevent = new PixelmonCalcCritEvent(e, user, target,1.0D);
			Pixelmon.EVENT_BUS.post(critevent);
			
			return critevent.getCrit();
		}
	}

	public boolean canHit(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2)
	{
		return pixelmon2 != null && !pixelmon2.isFainted;
	}

	public static boolean canMovesHit(EntityPixelmon entity, EntityPixelmon target)
	{
		boolean[] b = new boolean[4];
		int i1 = 0;
		b[0] = b[1] = b[2] = b[3] = true;

		for (int i = 0; i < entity.getMoveset().size(); ++i)
		{
			Attack a = entity.getMoveset().get(i);
			if (!a.canHit(entity, target))
			{
				b[i1] = false;
			}

			++i1;
		}

		return b[0] && b[1] && b[2] && b[3];
	}

	public static int getAttackCategory(int categoryID)
	{
		if (categories == null)
		{
			categories = DatabaseMoves.getAttackCategories();
		}

		String categoryString = "";
		AttackCategory[] arg1 = categories;
		int arg2 = arg1.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			AttackCategory category = arg1[arg3];
			if (category.index == categoryID)
			{
				categoryString = category.category;
			}
		}

		if (categoryString.equalsIgnoreCase("Special"))
		{
			return 1;
		}
		else if (categoryString.equalsIgnoreCase("Physical"))
		{
			return 0;
		}
		else if (categoryString.equalsIgnoreCase("Status"))
		{
			return 2;
		}
		else
		{
			if (PixelmonConfig.printErrors)
			{
				Pixelmon.LOGGER.info("Unknown attack category: " + categoryString);
			}

			return -1;
		}
	}

	public boolean doesPersist(EntityPixelmon entityPixelmon)
	{
		int i;
		if (this.isAttack(new String[] { "Fly", "Bounce" }))
		{
			for (i = 0; i < entityPixelmon.getStatusSize(); ++i)
			{
				StatusBase arg5 = entityPixelmon.getStatus(i);
				if (arg5.type == StatusType.Flying)
				{
					return true;
				}
			}

			return false;
		}
		else
		{
			for (i = 0; i < this.baseAttack.effects.size(); ++i)
			{
				EffectBase e = (EffectBase) this.baseAttack.effects.get(i);

				try
				{
					if (e.doesPersist(entityPixelmon))
					{
						return true;
					}
				}
				catch (Exception arg4)
				{
					entityPixelmon.battleController.battleLog.onCrash(arg4, "Error in doesPersist for " + e.getClass().toString() + " for attack " + this.baseAttack.getLocalizedName());
				}
			}

			for (i = 0; i < entityPixelmon.getStatusSize(); ++i)
			{
				if (entityPixelmon.getStatus(i).type == StatusType.Recharge)
				{
					return true;
				}
			}

			return false;
		}
	}

	public boolean cantMiss(PixelmonWrapper user)
	{
		if (this.cantMiss)
		{
			return true;
		}
		else
		{
			for (int i = 0; i < this.baseAttack.effects.size(); ++i)
			{
				EffectBase e = (EffectBase) this.baseAttack.effects.get(i);

				try
				{
					if (e.cantMiss(user))
					{
						return true;
					}
				}
				catch (Exception arg4)
				{
					user.bc.battleLog.onCrash(arg4, "Error in cantMiss for " + e.getClass().toString() + " for attack " + this.baseAttack.getLocalizedName());
				}
			}

			return false;
		}
	}

	public static String getCategoryString(int category)
	{
		return category == 0 ? I18n.translateToLocal("attack.category.physical") : (category == 1 ? I18n.translateToLocal("attack.category.special") : (category == 2 ? I18n.translateToLocal("attack.category.status") : ""));
	}

	public void sendEffectiveChat(PixelmonWrapper user, PixelmonWrapper target)
	{
		String s = null;
		if (this.baseAttack.attackCategory != 2)
		{
			float effectiveness = (float) this.getTypeEffectiveness(user, target);
			if (effectiveness == 0.0F)
			{
				user.bc.sendToAll("pixelmon.battletext.noeffect", new Object[] { target.pokemon.getNickname() });
				return;
			}

			if (effectiveness != 0.5F && effectiveness != 0.25F)
			{
				if (effectiveness == 2.0F || effectiveness == 4.0F)
				{
					s = "pixelmon.battletext.supereffective";
				}
			}
			else
			{
				s = "pixelmon.battletext.wasnoteffective";
			}

			if (s != null)
			{
				if (user.targets.size() > 1)
				{
					user.bc.sendToAll(s.concat("target"), new Object[] { target.pokemon.getNickname() });
				}
				else
				{
					user.bc.sendToAll(s, new Object[0]);
				}
			}
		}

	}

	public static boolean dealsDamage(Attack attack)
	{
		return attack != null && attack.baseAttack.basePower > 0;
	}

	public void saveAttack()
	{
		this.savedPower = this.baseAttack.basePower;
		this.savedAccuracy = this.baseAttack.accuracy;
		this.savedType = this.baseAttack.attackType;
	}

	public void restoreAttack()
	{
		this.baseAttack.basePower = this.savedPower;
		this.baseAttack.accuracy = this.savedAccuracy;
		this.baseAttack.attackType = this.savedType;
	}

	public boolean isAttack(String... attacks)
	{
		String[] arg1 = attacks;
		int arg2 = attacks.length;

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			String a = arg1[arg3];
			if (this.baseAttack.getUnLocalizedName().equalsIgnoreCase(a))
			{
				return true;
			}
		}

		return false;
	}

	public static boolean hasAttack(List<Attack> attackList, String... attackNames)
	{
		Iterator arg1 = attackList.iterator();

		Attack attack;
		do
		{
			if (!arg1.hasNext())
			{
				return false;
			}

			attack = (Attack) arg1.next();
		}
		while (attack == null || !attack.isAttack(attackNames));

		return true;
	}

	public static boolean hasOffensiveAttackType(List<Attack> attackList, EnumType type)
	{
		Iterator arg1 = attackList.iterator();

		Attack attack;
		do
		{
			if (!arg1.hasNext())
			{
				return false;
			}

			attack = (Attack) arg1.next();
		}
		while (attack == null || attack.baseAttack.attackType != type || attack.baseAttack.attackCategory == 2);

		return true;
	}

	public void createMoveChoices(PixelmonWrapper pw, ArrayList<MoveChoice> choices, boolean includeAllies)
	{
		ArrayList targets = new ArrayList();
		TargetingInfo info = this.baseAttack.targetingInfo;
		if (info.hitsSelf)
		{
			targets.add(pw);
		}

		if (info.hitsAdjacentAlly && (includeAllies || info.hitsAll))
		{
			targets.addAll(pw.bc.getTeamPokemonExcludeSelf(pw));
		}

		if (info.hitsAdjacentFoe)
		{
			targets.addAll(pw.bc.getOpponentPokemon(pw));
		}

		if (info.hitsAll)
		{
			choices.add(new MoveChoice(pw, this, targets));
		}
		else
		{
			Iterator arg5 = targets.iterator();

			while (arg5.hasNext())
			{
				PixelmonWrapper target = (PixelmonWrapper) arg5.next();
				ArrayList targetArray = new ArrayList(1);
				targetArray.add(target);
				choices.add(new MoveChoice(pw, this, targetArray));
			}
		}

	}

	public ArrayList<MoveChoice> createMoveChoices(PixelmonWrapper pw, boolean includeAllies)
	{
		ArrayList choices = new ArrayList();
		this.createMoveChoices(pw, choices, includeAllies);
		return choices;
	}

	public ArrayList<Attack> createList()
	{
		ArrayList list = new ArrayList(1);
		list.add(this);
		return list;
	}

	public boolean equals(Object compare)
	{
		return compare != null && compare instanceof Attack ? this.baseAttack.getUnLocalizedName().equalsIgnoreCase(((Attack) compare).baseAttack.getUnLocalizedName()) : false;
	}

	public int hashCode()
	{
		return this.baseAttack == null ? 0 : this.baseAttack.getUnLocalizedName().hashCode();
	}

	public String toString()
	{
		return this.baseAttack.getUnLocalizedName();
	}

	public double getTypeEffectiveness(PixelmonWrapper user, PixelmonWrapper target)
	{
		ArrayList effectiveTypes = target.pokemon.getEffectiveTypes(user.pokemon, target.pokemon);
		double effectiveness = (double) EnumType.getTotalEffectiveness(effectiveTypes, this.baseAttack.attackType);

		EffectBase e;
		for (Iterator arg5 = this.baseAttack.effects.iterator(); arg5.hasNext(); effectiveness = e.modifyTypeEffectiveness(effectiveTypes, this.baseAttack.attackType, effectiveness))
		{
			e = (EffectBase) arg5.next();
		}

		return effectiveness;
	}

	public boolean canRemoveBerry()
	{
		return this.isAttack(new String[] { "Bug Bite", "Pluck", "Knock Off" });
	}
}